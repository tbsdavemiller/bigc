
module.exports = {
  'Suppliers': {
    all: 1297,
    'Smith': 2138,
    'SmartE': 2476,
    'Trek': 2130,
    'Cervelo': 2133,
    'Thule': 2131,
    'Fox Easton': 2167,
    'Fox Racing': 2136,
    'Giant': 1731,
    'Hawley Lambert Cycling': 1730,
    'HighwayTwo': 2134,
    'NRG': 2135,
    'Live to Play Sports': 1576,
    'Outdoor Gear Canada': 2037,
    'Orange Sport Supply': 1733,
    'Shimano': 1732,
    'Specialized': 1298,
    'Troy Lee Designs': 2139,
    'Rocky Mountain Bicycles': 1527
  },
  'Bikes': {
    all: 1284,
    'Mountain': {
      all: 1289,
      'Hardtail': 1773,
      'Dual Suspension': 1776,
      'Fat Bikes': 2178,
      'Frames': 1777
    },
    'Road & Gravel': {
      all: 1290,
      'Road': 1779,
      'Gravel': 1780,
      'Cyclocross': 1778,
      'Frames': 1782
    },
    'Electric': {
      all: 1291,
      'Mountain - Hardtail': 1783,
      'Mountain - Dual Suspension': 2419,
      'Road': 1784,
      'Hybrid': 1785,
      'Cargo': 2419
    },
    'Hybrid': {
      all: 1292,
      'Fitness': 1786,
      'Comfort': 1787,
      'Cruiser': 1788,
      'Cargo': 2420
    },
    'Kids\' Bikes': {
      all: 1293,
      '12-Inch (2-4 yr. old)': 1789,
      '16-Inch (3-6 yr. old)': 2421,
      '20-Inch (5-8 yr. old)': 1792,
      '24-Inch & Up (7+ yr. old)': 1793
    }
  },
  Components: {
    all: 1285,
    Drivetrain: {
      all: 1806,
      Cassettes: 1808,
      'Chains & Guides': 1810,
      Derailleurs: 1813,
      'Full Groupsets': 1815,
      'Cranksets & Chainrings': 1811,
      Shifters: 1817,
      'Bottom Brackets': 1807,
      'Electronic Components': 1814,
      'Small Parts': 1812
    },
    Brakes: {
      all: 1818,
      'Brake Sets': 2047,
      Levers: 1821,
      Pads: 1822,
      Rotors: 2422,
      'Bleed Kits & Fluids': 1823,
      'Brake Calipers': 1820,
      'Cables & Hoses': 2424,
      'Small Parts': 2423
    },
    'Wheels, Tires & Tubes': {
      all: 1824,
      Tires: 1825,
      Tubes: 1832,
      'Wheels & Rims': 1826,
      'Hubs & Parts': 1827,
      'Tubeless Kits & Parts': 1833,
      'Spokes & Nipples': 1890,
      'Small Parts': 1828
    },
    'Forks & Suspension': {
      all: 1834,
      Forks: 1835,
      'Fork Parts & Oil': 1836,
      Shocks: 1837,
      'Shock Parts & Oil': 1838,
      'Linkage Parts': 1839
    },
    'Cockpit, Saddles & Pedals': {
      all: 1840,
      'Pedals & Cleats': 1858,
      'Grips & Tape': 1841,
      Saddles: 1863,
      Handlebars: 1845,
      'Seatposts & Levers': 1864,
      Stems: 1870,
      Headsets: 1852,
      'Seatpost Parts & Clamps': 1867,
      'Small Parts': 1868
    }
  },
  Accessories: {
    all: 1286,
    'Tools & Maintenance': {
      all: 1911,
      'Lubes, Cleaners & Sealant': 1912,
      Tools: 1914,
      Workstands: 1915,
      Storage: 1913
    },
    'Transport & Child Carriers': {
      all: 1916,
      'Car Racks': 1919,
      'Tailgate Pads': 1923,
      'Bike & Gear Bags': 1918,
      'Child Trailers & Accessories': 1922,
      'Fork Mounts & Small Parts': 1917
    },
    'Electronics & Lights': {
      all: 1925,
      'Mounts & Sensors': 2524,
      'Computers': 1926,
      'Lights': 1935,
      'Power Meters': 1937,
      'Mounts & Sensors': 1940,
      'Mounts': 1940,
      'Sensors': 2060,
      'Cameras': 2060,
      'Small Parts': 2425
    },
    'On-Bike Essentials': {
      all: 1941,
      'Bags': 1942,
      'Baskets & Racks': 1952,
      'Locks': 1956,
      'Bells': 1953,
      'Fenders': 1954,
      'Kickstands': 1955,
      'Mirrors': 1963,
      'Kids\' Accessories': 1967
    },
    'Hydration & Inflation': {
      all: 1968,
      'Bottles & Cages': 1973,
      'Hydration Packs': 1971,
      'Pumps & Gauges': 1980,
      'CO2 Inflators & Cartridges': 1977,
      'Coolers & Drinkware': 1972
    },
  },
  Training: {
    all: 2239,
    Trainers: {
      all: 2213,
      'Trainer Bikes': 1796,
      'Smart Trainers': 1795,
      'Wheel On Trainers': 2426,
      Rollers: 2427
    },
    'Trainer Accessories': {
      all: 1798,
      'Trainer Mats': 2428,
      'Trainer Fans': 2429,
      'Stands & Desks': 2430,
      'Parts & Connectivity': 2431
    },
    'Nutrition & Performance': {
      all: 2214,
      'Bars & Snacks': 2217,
      'Hydration': 2218,
      Gels: 2219
    },
    'Recovery & Health': {
      all: 2432,
      'Recovery Supplements': 2433,
      'Recovery Equipment': 2434,
      'Chamois cream': 2435
    }
  },
  Apparel: {
    all: 1287,
    Men: {
      all: 1994,
      Tops: 1995,
      Bottoms: 1996,
      Outerwear: 2000,
      Gloves: 1999,
      Protection: 2002,
      Socks: 2001,
      'Warmers & Caps': 2036,
      Baselayers: 1997,
      'Casual, Hats & Gifts': 1998
    },
    Women: {
      all: 2003,
      Tops: 2004,
      Bottoms: 2005,
      Outerwear: 2009,
      Gloves: 2008,
      Protection: 2183,
      Socks: 2010,
      'Warmers & Caps': 2011,
      'Baselayers': 2006,
      'Casual, Hats & Gifts': 2007
    },
    Youth: {
      all: 2026,
      Tops: 2027,
      Bottoms: 2028,
      Outerwear: 2029,
      Gloves: 2215,
      Protection: 2035,
      Socks: 2184,
      'Casual, Hats & Gifts': 2436
    },
    'Helmets & Eyewear': {
      all: 2020,
      'Mountain': 2021,
      'Road': 2022,
      'Kids': 2023,
      'Sunglasses & Goggles': 2437,
      'Accessories & Parts': 2024
    },
    'Shoes': {
      all: 2012,
      Mountain: 2013,
      Road: 2014,
      'Fatbike & Winter': 2181,
      'Footbeds': 2015,
      'Small Parts': 2019
    },
    'The Bike Shop Custom': {
      all: 2064,
      Jerseys: 2209,
      Shorts: 2438,
      'Hats & Casual': 2439,
      Bottles: 2066,
      'Stickers & Other': 2065
    }
  },
  featured: {
    all: 2124,
    financing: 2126,
    gift: 2128,
    arrivals: 2125,
    sales: 2127
  }
}
