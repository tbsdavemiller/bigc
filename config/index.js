const env = {
  prod: {
    domain : 'the-bike-shop8.mybigcommerce.com',
    id : 'gtyokp3oxr',
    client : '10t17649mh4pz1z5z6cw6t0b4er0pxt',
    secret : 'be3b1910116c0fced683d237601fbd02985d62caa2e4ea05572a507c6769d986',
    token : 'qse8ov3g2fj5t0owri2k4s1hkzts7jr'
  },
  sandbox : {
    domain: 'the-bike-shop-sandbox.mybigcommerce.com',
    id : 'hnktbeyuzy',
    client : '4xtsnub8qb9bdfc8r5srd8hm63s4cm8',
    secret : '4f7ece7486d441e491fb9e994448c9d2811266361a5c73b5fdd0be0ac848adab',
    token : 'kbobykddcxg6mgg9c0to41w0irnqtgw'
  }
};


module.exports = {
  api: {
    domain: 'https://api.bigcommerce.com/stores',
    headers: key => {
//      console.log('env', key);
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Auth-Token': env[key].token,
        'X-Auth-Client': env[key].client
      }
    }
  },
  env: env,
  categories: require('./_categories'),
  brands: require('./_brands')

};
