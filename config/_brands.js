
module.exports = {
  abus: 303, // hlc
  amphuman: 422,
  //apidura: ,
  bell: 235,
  bikase: 0,
  blackburn: 236, // ogc
  bontrager: 336, // trek
  bosch: 177, //
  bsd: 23729, // ogc
  camelbak: 239, // ogc
  campagnolo: 282,
  cervelo: 327,
  chromag: 349,
  cinema: 23735, // ogc
  continental: 126, // ogc
  crankbros: 23690,
  cushcore: 125,
  deity: 333,
  dtswiss: 170, // ogc
  easton: 408,
  eclat: 23724, // ogc
  electra: 278,
  elite: 91,
  evoc: 304, // hlc
  federal: 23730, // ogc
  fiend: 23732, // ogc
  finishline: 252, // ogc
  fitbikeco: 23731, // ogc
  fiveten: 197,
  fortyfivenorth: 198,
  fox: 253,
  foxracing: 402,
  fyxation: 255, //
  garmin: 300,
  giant: 225,
  giro: 227, //
  hammerhead: 29356,
  hundop: 76,
  kedge: 291, // hlc
  kenda: 121,
  kink: 257, // ogc
  lezyne: 283, // hlc
  linus: 231, // ogc
  maxxis: 208, // hlc
  merritt: 208, // ogc
  mucoff: 221, // hlc
  niterider: 92, // smarte
  niteize: 23675, // trek
  odi: 134, // hlc
  ohlins: 241, // ogc
  oneup: 23741,
  osprey: 2130, // trek
  parktool: 185,
  pedros: 316, // hlc
  pinhead: 23698, // trek
  pirelli: 209,
  polar: 23686, // trek
  pro: 418,
  profile: 244, // ogc
  proviz: 297, // hlc
  raceface: 409,
  'radio-raceline': 23728,
  rapha: 29382, 
  rockshox: 180, // hlc
  rocky: 226,
  roswheel: 306, //
  salt: 23726, // ogc
  saltplus: 23727, // ogc
  smartwool: 23740,
  schwalbe: 56,
  shimano: 284,
  smith: 406,
  specialized: 223,
  's&m-bikes': 23733, // ogc
  sram: 154,
  stages: 249, // ogc
  //stans: , // ogc
  supacaz: 57,
  tacx: 279, // hlc
  tallorder: 23736, // ogc
  terrene: 123, // smarte
  time: 242,
  topeak: 130,
  trek: 277,
  'trek-diamant': 23679,
  troylee: 411,
  unior: 346, // trek
  thule: 229,
  vittoria: 230, // ogc
  wahoo: 146,
  wethepeople: 23725, // ogc
  wheels: 71, // hlc
  wolftooth: 288,
  yakima: 112,
  yeti: 29364,
  yeticycles: 23743,
  zefal: 214, // hlc

}
