const
  config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  requestPromise = require('request-promise');
  catalog = require('./catalog.json');

const
  key = 'dd4882af3b7a96de5c32fad8d241b083',
  domain = 'https://clsdev.qbp.com/api3/1/product';

if (process.argv.length < 3) {
  console.error('🚫 no index');
  return;
}

const obj = {
  url: domain,
  json: true,
  body: { "codes": [ process.argv[2] ] },
  method: 'POST',
  headers: {
    'X-QBPAPI-KEY': key,
    'accept': 'application/json',
    'Content-Type': 'application/json'
  }
};



requestPromise(obj)
  .then(response => {
    const products = response.products;

    console.log(products);
  })
  .catch(e => {
    console.log(e);
  });



// for (let i = 0; i < bits.length; i++) {
//
//   const obj = {
//     url: domain,
//     json: true,
//     body: { "codes": [ "RM1673" ] },
//     method: 'POST',
//     headers: {
//       'X-QBPAPI-KEY': key,
//       'accept': 'application/json',
//       'Content-Type': 'application/json'
//     }
//   };
//
// //  promises[] =
//   requestPromise(obj)
//     .then(response => {
//       const products = response.products;
//
//   //    console.log(response.products[0]);
//     })
//     .catch(e => {
//       console.log(e);
//     });
// }
