const
  config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  requestPromise = require('request-promise'),
  objectsToCsv = require('objects-to-csv'),
  catalog = require('./catalog.json');

const
  key = 'dd4882af3b7a96de5c32fad8d241b083',
  domain = 'https://clsdev.qbp.com/api3/1/product',
  window = 100;

function flatten(data) {
  var result = {};
  function recurse (cur, prop) {
    if (Object(cur) !== cur) {
      result[prop] = cur;
    } else if (Array.isArray(cur)) {
      for(var i=0, l=cur.length; i<l; i++) {
        recurse(cur[i], prop + "[" + i + "]");
      }
      if (l == 0) {
        result[prop] = [];
      }
    } else {
      var isEmpty = true;
      for (var p in cur) {
        isEmpty = false;
        recurse(cur[p], prop ? prop+"."+p : p);
      }
      if (isEmpty && prop) {
        result[prop] = {};
      }
    }
  }
  recurse(data, "");
  return result;
}

function chunk (items, size) {
  const chunks = []
  items = [].concat(...items);

  while (items.length) {
    chunks.push(
      items.splice(0, size)
    );
  }

  return chunks;
}

const bits = chunk(catalog, window);
console.log(bits.length);

if (process.argv.length < 3) {
  console.error('🚫 no index');
  return;
}


const
  index = parseInt(process.argv[2], 10);







const promises = [];

let filtered = [];

//for (let j = 0; j < bits.length; j++) {

//  const index = j;

  const ids = [];
  for (let i = 0; i < bits[index].length; i++) {
    ids.push(bits[index][i]);
  }
  filtered = filtered.concat(...ids);

  const obj = {
    url: domain,
    json: true,
    body: { 'codes': ids },
    method: 'POST',
    headers: {
      'X-QBPAPI-KEY': key,
      'accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };
  promises.push(requestPromise(obj));
//}


Promise.all(promises)
  .then(list => {
    const flattened = [];

    list.forEach(l => {

      const products = l.products;
      products.forEach(p => {
        filtered = filtered.filter(code => { return code != p.code; });

        const simple = {
          mpn: p.mpn,
          name: p.name,
          attributes: p.productAttributes
        };

        flattened.push(flatten(simple));
      });
    });

    new objectsToCsv(flattened).toDisk('/Users/dave/Desktop/qbp.csv', { append: true, bom: true, allColumns: true})
      .then(s => {
        console.log('✅');
      })
      .catch(e => {
        console.error(e);
      });

      console.error(`missing ${filtered.length}: ${JSON.stringify(filtered)}`);

//console.log(p);
  })
  .catch(e => {
    console.error(e);
  });

//
//   .then(response => {
//     const products = response.products;
//
//     const flattened = [];
//
//     products.forEach(p => {
//       filtered = filtered.filter(code => { return code != p.code; });
//
//       const simple = {
//         mpn: p.mpn,
//         name: p.name,
//         attributes: p.productAttributes
//       };
//
//       flattened.push(flatten(simple));
//     });
//     console.error(`missing ${filtered.length}: ${JSON.stringify(filtered)}`);
//
//     new objectsToCsv(flattened).toString(true, true)
//       .then(s => {
//         console.log(s);
//       })
//       .catch(e => {
//         console.error(e);
//       });
//
//
//   })
//   .catch(e => {
//     console.log(e);
//   });
//
// }
//
//
//
//
//
