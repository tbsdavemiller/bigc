const
  config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  request = require('request');

const csv = require('node-csv').createParser();

csv.parseFile(path.join(__dirname, '..', 'config', 'rapha.csv'), function(err, rows) {

  if (err) {
    console.error(err);
    return;
  }

  const start = 672;
  const count = 40;

  for (let i = start; i < start + count; i++) {

console.log(i);

    if (i >= rows.length) {
    console.log('longer');
      break;
    }

    const row = rows[i];

    let
      id = row[0],
      style = row[1];

    style = style.replace(/[\/ ]/ig, '-');

    const images = row.splice(2).filter(r => r != '');
    if (images.length == 0) {
      console.error(`no images for ${i}`);
      continue;
    }

    for (let img = 0; img < images.length; img++) {

      const dir = `./imgs/${id}-${style}`;

      if (! fs.existsSync(dir)) {
          fs.mkdirSync(dir, true);
      }

      const url = images[img];

      let obj = {
        headers: {},
        encoding:'binary'
      };

      request.get(url,obj, (e, response, body) => {
        if (e) {
          console.error(i, e);
          return;
        }
        if (response.statusCode === 200) {
          const f = path.join(dir, url.split('/').splice(-2, 1)[0] + '.jpg');
          fs.writeFile(f,body, 'binary', e => {
            if (e) {
              console.error(url, e);
            }
          });

        } else {
          console.error(url, response.statusCode);
        }
      });
    }
  }
});
