const
  config = require('./config');
  requestPromise = require('request-promise'),
  request = require('request');

const key = 'prod';
const env = config.env[key];


const getBrand = id => {

  return {
    url: `${config.api.domain}/${env.id}/v3/catalog/brands/${id}`,
    json: true,
    method: 'get',
    headers: config.api.headers(key)
  };
};

/**
 *
 */
const getProducts = function(brand, category, page) {

  return new Promise(
    function(resolve, reject) {

      let url = `${config.api.domain}/${env.id}/v3/catalog/products?brand_id=${brand}` + (category ? `&categories:in=${category}` : '') + (page ? `&page=${page}` : '');
      request({
        headers: config.api.headers(key),
        json: true,
        uri: url,
        method: 'GET',
      },
      function(err, res, body) {
        if (err || res.statusCode != 200){
//          console.log(url, body);
          reject(err || { statusCode: res.statusCode });
          return;
        }
        resolve(body);
      });
    }
  )
};

async function getAllProducts(brand, category) {

  let page = 1;
  let res = await getProducts(brand, category, page);
  let all = [];
  res.data.forEach(b => {
    all.push(b);
  });

  while (res.meta.pagination.current_page < res.meta.pagination.total_pages) {
    page++;
    res = await getProducts(brand, category, page);
    res.data.forEach(b => {
      all.push(b);
    });
  }
  return all;
}

const brand = process.argv.length > 2 ? process.argv[2] : null;
const category = process.argv.length > 3 ? process.argv[3] : null;

async function meta(brand, category) {

  return new Promise(
    function(resolve, reject) {

      request(
        getBrand(brand),
        function(err, res, body) {
          if (err || res.statusCode != 200) {
            console.log(url, body);
            reject(err || { statusCode: res.statusCode });
            return;
          }

          let prods = getProducts(brand, category, null).then(prods => {
            resolve({ brand: body.data, prods: prods.meta.pagination.total });
          }).catch(e => {
            reject(e);
          });
        }
      );
    }
  );
}

if (! brand) {

  Promise.all(Object.keys(config.brands).map(b => {
    return meta(config.brands[b])
  })).then(values => {
    const sorted = values.sort((a,b) => { return a.brand.name < b.brand.name; });
    sorted.forEach(s => {
      console.log(s.brand.id, s.brand.name, s.prods );
    });
  });
  return;
}

// is id a number?
let parsed = parseInt(brand, 10);
const arg = `${parsed}` === brand ? brand : config.brands[brand];

requestPromise(getBrand(arg))
.then((response) => {

  const products = getAllProducts(arg, category);

  products.then((data) => {

    const sorted = data.sort((a,b) => { return a.name - b.name; });

    console.log(`${response.data.name} (${data.length}) : https://${env.domain}${response.data.custom_url.url}`);

    console.log(data.map(b => `  ${b.sku ? b.sku : (b.upc ? b.upc : b.id)} : ${b.is_visible ? "👁" : "👁‍🗨"}  ${b.name} : ${b.categories.join(', ')}`).join("\n"));
  }).catch(e => {
    console.log(e);
  });
})
.catch(e => {
  console.error(`🚫 ${e}`);
});
;

return;
