const
  path = require('path'),
  config = require(path.join(__dirname, '..', 'config')),
  csvToJson = require('csvtojson'),
  create = require(path.join(__dirname, '..', 'create')),
  _ = require('lodash');

const key = 'sandbox';

const products = [
  _.cloneDeep(require('./schema.js')),
  _.cloneDeep(require('./schema.js'))
];

products[1].name = "mug #2";
products[1].variants[0].sku = "mug #2";

products.forEach(p => {
  create(p, key)
    .then(product => {
      console.log(JSON.stringify(product, null, '  '));
    }).
    catch(e => {
      console.log(`🚫 ${p.name}`, e);
    });
});
