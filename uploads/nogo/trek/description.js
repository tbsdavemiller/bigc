const
  config = require('../../config'),
//   requestPromise = require('request-promise'),
//   request = require('request'),
  csvToJson = require('csvtojson'),
  path = require('path'),
  fs = require('fs');
//  upload = require(path.join(__dirname, '..', '..', 'product'));



const markup = row => {
  let markup = '';

  let features = '';
//   let position = '';

  markup = `<p>${row['Positioning Statement']}</p>\n`;

  // aftermarket
  if (row['Headline']) {

    if (row['Key Feature 1']) {
      features += `<p>${row['Key Feature 1']}</p>\n`;
    }

    if (row['Key Feature 2']) {
      features += `<p>${row['Key Feature 2']}</p>\n`;
    }

    if (row['Key Feature 3']) {
      features += `<p>${row['Key Feature 3']}</p>\n`;
    }

    if (row['Key Feature 4']) {
      features += `<p>${row['Key Feature 4']}</p>\n`;
    }

    if (row['Key Feature 5']) {
      features += `<p>${row['Key Feature 5']}</p>\n`;
    }

    if (row['Key Feature 6']) {
      features += `<p>${row['Key Feature 6']}</p>\n`;
    }

    if (row['Key Feature 7']) {
      features += `<p>${row['Key Feature 7']}</p>\n`;
    }

    if (row['Key Feature 8']) {
      features += `<p>${row['Key Feature 8']}</p>\n`;
    }

    let details = '';
    if (row['Overview 1']) {
      details += `<li>${row['Overview 1']}</li>\n`;
    }

    if (row['Overview 2']) {
      details += `<li>${row['Overview 2']}</li>\n`;
    }

    if (row['Overview 3']) {
      details += `<li>${row['Overview 3']}</li>\n`;
    }

    if (features) {
      markup += `\n<h4>${row['Headline']}</h4>\n` + features + '\n';
    }

    if (details) {
      markup += `<h4>Product Details</h4>\n<ul>\n${details}</ul>\n`;
    }


    return markup;
  }

  if (row['Key Feature 1']) {
    features += `<li>${row['Key Feature 1']}</li>\n`;
  }

  if (row['Key Feature 2']) {
    features += `<li>${row['Key Feature 2']}</li>\n`;
  }

  if (row['Key Feature 3']) {
    features += `<li>${row['Key Feature 3']}</li>\n`;
  }

  if (row['Key Feature 4']) {
    features += `<li>${row['Key Feature 4']}</li>\n`;
  }

  if (row['Key Feature 5']) {
    features += `<li>${row['Key Feature 5']}</li>\n`;
  }

  if (row['Key Feature 6']) {
    features += `<li>${row['Key Feature 6']}</li>\n`;
  }

  if (row['Key Feature 7']) {
    features += `<li>${row['Key Feature 7']}</li>\n`;
  }

  if (row['Key Feature 8']) {
    features += `<li>${row['Key Feature 8']}</li>\n`;
  }

  if (features) {
    markup += `\n<h4>Why You'll Love It</h4>\n<ul>\n${features}</ul>\n`;
  }

  if (row['Overview 1']) {
    markup += `\n<h4>It's right for you if…</h4>\n<p>${row['Overview 1']}</p>\n`;
  }

  if (row['Overview 2']) {
    markup += `\n<h4>The tech you get</h4>\n<p>${row['Overview 2']}</p>\n`;
  }

  if (row['Overview 3']) {
    markup += `\n<h4>The final word</h4>\n<p>${row['Overview 3']}</p>\n`;
  }

  return markup;
};



// const after= csvToJson({
//   delimiter: '\t',
//   trim:true
// }).fromFile(__dirname + '/data/aftermarket.tsv');
//
// after.then(rows => {
//   let i = 0;
//   rows.forEach(row => {
//     if (i === 3) { // rows.length - 1
//       console.log(markup(row));
//     }
//     i++;
//   });
// });
//
// return;

const bikes= csvToJson({
  delimiter: '\t',
  trim:true
}).fromFile(__dirname + '/data/bikes.tsv');

bikes.then(rows => {
  let i = 0;
  rows.forEach(row => {
    if (i === rows.length -1 ) {
      console.log(markup(row));
    }
    i++;
  });
});

//
// rows.forEach(r => {
//   console.log(r);
// });
