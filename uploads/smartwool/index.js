const
  config = require('../../config'),
  requestPromise = require('request-promise'),
//   request = require('request'),
  csvToJson = require('csvtojson'),
  path = require('path'),
  sleep = require('../../utils/sleep'),
  search = require(path.join(__dirname, '..', '..', 'search')),
  del = require(path.join(__dirname, '..', '..', 'delete')),
  create = require(path.join(__dirname, '..', '..', 'product'));

const
  BRAND = 42,
  ENV = 'sandbox';

const products = [];

const findProductForSKU = (sku) => {
  let p = products.find(p => {
    let v = p.skus.find(v => { return v === sku; })
    return v ? p : null;
  });
  return p;
};



const processRows = async () => {

  const rows = await csvToJson({
    trim:true
  }).fromFile(__dirname + '/products.csv');

  rows.forEach(row => {

    if (row.style.trim().length > 0) {
      products.push({
        name: row.style,
        description: `${row.style} description`,
        type: 'physical',
        inventory_tracking: 'variant',
        categories: [294],
        brand_id: BRAND,
        photos: [],
        meta: [
          {
            key: 'meta field name 1',
            value: 'meta field value',
          },
          {
            key: 'meta field name 2',
            value: 'meta field value',
          }
        ],
        custom: [
          {
            name: 'foo',
            value: 'bar'
          }
        ],
        skus: []
      });
    } else {
      products[products.length - 1].skus.push(row.SKU);
    }
  });

  const variants = await csvToJson({
    trim:true
  }).fromFile(__dirname + '/variants.csv');

  let variantIndex = 0;
  variants.forEach(v => {
    const product = findProductForSKU(v.VPN);
    if (! product) {
      console.error(`no product for ${v.VPN}`);
      return;
    }

    const obj = {
      sku: v.VPN,
      retail_price: parseFloat(v.MSRP.replace('$', '')),
      cost_price: parseFloat(v.EstimatedCost.replace('$', '')),
      option_values: [
        {
          option_display_name: "Color",
          label: v.Color
        },
        {
          option_display_name: "Size",
          label: v.Size
        }
      ],
      meta: [
        {
          key: 'meta field name 1',
          value: 'meta field value',
        },
        {
          key: 'meta field name 2',
          value: 'meta field value',
        }
      ]
    };

    if (v.Images) {
      obj.image_url = 'https://thebikeshopracing.s3.amazonaws.com/tbs/' + v.Images;
    }

    if (! product.variants) {
      product.variants = [obj];
    } else {
      product.variants.push(obj);
    }
    product.price = product.variants[0].retail_price;
    product.weight = 0.5;
    variantIndex += 1;
  });

  return products;
};


requestPromise(search("", {env: ENV, brand: BRAND }))
.then((response) => {

  const promises = [];

  const rows = response.data;
  if (rows.length) {
    console.log(`Deleting ${rows.length} product${rows.length > 1 ? 's' : ''}`);
    rows.forEach(r => {
  //    console.log(`${r.id} sku ${r.sku} : ${r.is_visible ? "👁" : "👁‍🗨"}  ${r.name} 📂 ${r.categories.join(' ')}`);
      promises.push(
        del({ id: r.id }, { env: ENV })
      );
    });
  }
  return Promise.all(promises);
})
.then(() => { return sleep(2000); })
.then(() => { return processRows() })
.then(rows => {
  let i = 0;
  rows.forEach(r => {
    delete (r.skus);

    if (i > 2) {
      return;
    }

    let photo = null
    r.variants.forEach(v => {
      if (photo !== null || v.image_url === null) {
        return;
      }
      r.photos = [{
        is_thumbnail: false,
        image_url: v.image_url
      }];
    });

    create(r, { env: ENV })
      .then(product => {
        console.log('✅ ' + product.name);
      })
      .catch(e => {
        console.error('🚫 ' + r.name + ':', e);
      });

    i++;
  });

});
