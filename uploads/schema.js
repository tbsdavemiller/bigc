/*
  A template object for example product to be uploaded
*/

module.exports = {
  "name": "BigCommerce Coffee Mug",
  "price": "10.00",
  "weight": 4,
  "type": "physical",
  "description": "<p>this mug is sick</p>",
  "categories": [27],
  "brand_id": 39,
  "inventory_tracking": "variant",
/*
  "custom": [
    {
      "name": "foo",
      "value": "bar"
    },
    {
      "name": "buh",
      "value": "hub"
    }
  ],
  "photos": [
    {
      // product_id: ###
      "is_thumbnail": false,
      // "sort_order": #,
      "image_url": "https://store-gtyokp3oxr.mybigcommerce.com/product_images/c/000/SantiniTrekSegafredoWomensTeamSupportersReplicaJers_32881_A_Primary__72865.jpg",
      "description": "this is a mug"
    },
    {
      // product_id: ###
      "is_thumbnail": false,
      // "sort_order": #,
      "image_url": "https://store-gtyokp3oxr.mybigcommerce.com/product_images/c/000/SantiniTrekSegafredoWomensTeamSupportersReplicaJers_32881_A_Primary__72865.jpg",
      "description": "this is a another mug"
    }
  ],
*/
/*
  // https://developer.bigcommerce.com/api-reference/b3A6MzU5MDQzMDI-create-a-product-variant
  "variants": [
    {
      "cost_price": 10,
      "inventory_level": 10,
      "inventory_warning_level": 5,
      "sku": "AAA611-LR",
      "option_values": [
        {
          "option_display_name": "Color",
          "label": "Beige"
        },
        {
          "option_display_name": "Size",
          "label": "Small"
        }
      ]
    },
    {
      "cost_price": 10,
      "inventory_level": 10,
      "inventory_warning_level": 5,
      "sku": "AAA611-LS",
      "option_values": [
        {
          "option_display_name": "Color",
          "label": "Green"
        },
        {
          "option_display_name": "Size",
          "label": "Large"
        }
      ]
    }
  ]
*/
};
