# TBS APIs

(`$ROOT` refers to the directory containing this README file)

Ensure [nodejs](https://nodejs.org) is installed on your machine, then...

```
cd $ROOT
npm install
```

## `cleanup`

Move mis-categorized products into their proper locations in the category tree. The folders contain supplier-specific SKUs that need to be moved:

```
cd $ROOT/cleanup
node index.js [hlc|ogc|trek|...]
```

## `uploads`

A work in progress.

### Smartwool

```
cd uploads/smartwool
node index.js
```


### Big Commerce

* [API Docs](https://developer.bigcommerce.com/api-reference/f176e7aec547a-catalog)

### QBP
* [API](https://clsdev.qbp.com/api3/login)
* key: `dd4882af3b7a96de5c32fad8d241b083`

## OGC

* [API](https://pim.ogc.ca)
* API Key: `32913174bb85ecfaedd2937f6e6b2b21572a6da941f3e474bce9d7d8d6eca179`
