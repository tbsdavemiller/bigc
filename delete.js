const
  config = require('./config'),
  requestPromise = require('request-promise');

const del = (product, options) => {

  if (! options || ! options.env) {
    return new Promise((resolve, reject) => {
      reject('No env provided');
    });
  }

  const env = config.env[options.env];
  if (! env) {
    return new Promise((resolve, reject) => {
      reject(`Invalid env ${key}`);
    });
  }

  const deleteURL = product => {

    const obj = {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}`,
      json: true,
      method: 'delete',
      headers: config.api.headers(options.env)
    };
//    console.log('deleting', obj);
    return obj;
  };

//   const retrieveURL = (q, page) => {
//
//     const url = `${config.api.domain}/${env.id}/v3/catalog/products/?keyword=${q}${page ? '&page=' + page : ''}`;
//     return {
//       url: url,
//       json: true,
//       method: 'get',
//       headers: config.api.headers(key)
//     };
//   };


//   const updateURL = (product) => {
//
//     return {
//       url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}`,
//       json: true,
//       body: product,
//       method: 'put',
//       headers: config.api.headers(key)
//     };
//   };


  if (product.id) {
    return requestPromise(deleteURL(product));
  } else if (product.upc || product.sku) { }

};

module.exports = del;
