
const
//  tacx = require('./dist/tacx'),
  shimano = require('./dist/shimano'),
  sram = require('./dist/sram');


const tacxCats = {
  'tacx-trainer-bikes': [


  ],
  'tacx-trainer-smart': [

  ],
  'tacx-trainer-rollers': [
    'Tacx Antares rollers'
  ],
  'tacx-trainer-wheelon': [
    'Boost',
    'Boost Bundle',
  ],
  'tacx-trainer-mats': [

  ],
  'trainer-parts': [
    'Tacx, S0003, Rubber feet, Genius/Bushido/Satori/Boost',
    'Tacx, T1402, Quick release for rear wheel',
    'Tacx, T1902-03, Handlebar clamp'
  ],
  'tools': [
    'Wolf Tooth Pack Whip Chain Whip',
    'Park Tool SR-2.3 Sprocket Remover/Chain Whip',
    'Park Tool SR-12.2 Sprocket Remover Chain Whip',
    'Park Tool Sprocket Remover/Chain Whip',
    'Park Tool Master-Link Pliers',
    'Park Tool HCW-16.3 Chain Whip/Pedal Wrench',
    'Park Tool CM-5.3 Cyclone Chain Cleaner',
    'Park Tool CG-2.4 Chain Gang Chain Cleaning System',
    'Park Tool CC-4 Chain Checker',
    'Park Tool CC-3.2 Chain Wear Checker',
    'Park Tool CC-2 Chain Wear Checker',
    'Park Tool Bio ChainBrite',
    'Park Tool Keychain Bottle Opener'
  ],
  'cranks': [
    'Ultegra FC-R8100-P Power Meter',
  ],
  'components-drivetrain-parts': [
    'RoadLink DM',
    'Goatlink 11',
    '25mm B Pivot Screw',
    'Campagnolo, RD-RE900, Pulleys',
    'Campagnolo, Super Record 12 Pulleys, Campagnolo SR 12, Set',
    'Rival AXS B-Bolt/Limit Screws',
    'Force AXS Wide B-Bolt Kit',
    'XX1/X01 Eagle 52T B-Bolt/Limit Screw Kit',
    'GX Eagle 52T B-Bolt/Limit Screw Kit',
    'GX Eagle (52T) Inner Cage',
    'XX1 Eagle (52T) Inner Cage',
    'Red AXS Ceramic Pulley Kit',
    'SX Eagle Bolt and Screw Kit',
    'Red eTap AXS Front Derailleur Parts Kit',
    'eTap AXS Steel Bearing Pulley Kit',
    'Force AXS B-Bolt/B-Screw/Limit',
    'Red AXS B-Bolt/B-Screw/Limit',
    'XX1 Eagle AXS Bolt and Screw Kit',
    'X01 Eagle AXS Bolt and Screw Kit',
    'NX Eagle Bolt and Screw Kit',
    'NX Eagle Pulley Kit',
    'GX Eagle Pulley Kit',
    'GX Eagle Pulley and Inner Cage',
    'EX1 Inner Cage/Pulley',
    'XX1 B-Bolt and Limit',
    'Eagle Type 3 Cage Lock',
    'XX1/X01 Eagle Pulleys',
    'GX Eagle B Bolt and Limit Screw Kit',
    'X01 Eagle B-Bolt and Limit Screw Kit',
    'X01 Eagle Cable Anchor Kit',
    'X01 Eagle Pulley and Inner Cage',
    'Apex1/NX Pulley Kit',
    'NX 1x11 B-Adjust and Limit',
    'GX DH/X01 DH 7Sp B Bolt and Limit Screw Kit',
    'eTap Front Derailleur Parts',
    'Red eTap Pulleys',
    'eTap Limit Screws',
    'Red eTap B-Bolt/B-Screw',
    'Rival1 B-Adjust and Limit Kit',
    'GX 1x11 Inner Cage/Pulley',
    'GX 1x11 Cable Anchor Kit',
    'GX 1X11 B Bolt and Limit Screw Kit',
    'GX 2X11 Pulley Set',
    'GX 2x11 B-Bolt and Screw',
    'GX 2x10 B-Bolt and Screw',
    'Force 1 B-Bolt Kit',
    'X01 DH Screw Kit',
    'SRAM, X01DH B-Bolt Kit, 10 Speed',
    'X01 DH B-Bolt Kit',
    'X01/X01 DH/X1/CX1/GX1 X-Sync Pulley Set',
    'X01/X1 11sp Inner Cage/Pulley',
    'Cable Pulley for X01',
    'Force22 Pulley Set',
    'Red22 B-Bolt/B-Screw Kit',
    'X5 Pulley Set',
    'X7, GX Pulley Set',
    'X0 Type 2 Pulley Set',
    'XX1 Pulley Set',
    'XX1 Cable Anchor Kit',
    'XX1 B-Bolt Limit Screw Kit',
    'Red 13/22 Inner Cage',
    'Red Cable Adjuster Barrel',
    'Cable Anchor/Limit Screw for Red/22',
    'Aeroglide Ceramic Pulley Set',
    'SRAM Red Cable Adjuster Barrel',
    'Rival, Apex Pulley Set',
    'Rival Cage/Pulley Kit',
    'X9 B-Screw and Limit Kit',
    'X0/X9 Bolt Kit',
    'XX B-Bolt Kit',
    '2010-11 X7, X9 Pulley Set',
    'X9/X7 B-Bolt Kit',
    'RD Small Parts',
    'X0 Pulley Set',
    'Red B-Bolt Kit',
    'X9 Hanger Bolt',
    'RD Barrel Adjuster Kit',
    'Cable Anchor/Limit Screw for Rival',
    'X0, X9, X7 Pulley Set',
    'X5, X7 Pulley Set',
    '2005-09 X9 Pulley Set',
    'Dura Ace RD-R9100/R9150 Pulleys',
    'XTR/XT/SLX 11 Speed Cam Unit',
    '105 RD-5800 Pulleys',
    'XT RD-M773/M786 Pulley Set',
    'Ultegra RD-6800 Pulley Set',
    'Ultegra RD-6700 Pulleys',
    'Ultegra RD-R8000 / GRX RD-RX812 Pulleys',
    '105 RD-R7000 Pulleys',
    'SLX RD-M663 / 105 RD-5800 Pulleys',
    '105 RD-5700 Pulleys',
    'SLX RD-M7000 Pulleys',
    'XT RD-M8000 Pulleys',
    'Tourney Pulleys',
    'Look, Internal cable routing plugs for SRAM ETAP, For 795/ 695/ 675/ 765',
    'Look, 785/795, Rear Derailleur Hanger',
    'Look, Cable Guide + Screw for 675 frames',
    'GRX RX817 Pulleys',
    'GRX RX815 Pulleys',
    'GRX RX400 Pulleys',
    'GRX RX810 Pulleys',
    'SLX RD-M7100 Pulleys',
    'XT RD-M8100 Pulleys',
    'XTR RD-M9100 Pulleys',
    'Freewheel / BB Spacer', // wheels
    'Cassette Spacer'
  ],
  'components-derailleurs': [
    'Campagnolo, Super Record, Front Derailleur, 12, Swing: Down, Cable Pull: Down, Braze-on',
    'Rd-M8120 Deore Xt Rear Derailleur (2x12)',
    'XX1 Eagle AXS',
    'Apex 1',
    'SRAM, NX, Rear derailleur, 11sp., Long cage, Black',
    'SRAM, X1, Rear derailleur, 11sp., Type 2.1, Long cage',
    'Force22',
    'Red',
    'X3',
    'X4',
    'Shimano, Altus FD-M310, Front derailleur, 3x7/8sp., Top swing, Dual pull, Multi clamp, For 42/48T',

// automagic
//     'Acera RD-T3000',
//     '105 FD-R7000',
//     'Dura Ace RD-R9100',
//     'XTR RD-M9100',
//     'Ultegra RX RD-RX800',
//     'SLX RD-M7000',
//     'Dura Ace Di2 FD-R9150',
//     'Dura Ace Di2 RD-R9150',
//     'Dura Ace FD-R9100',
//     'Ultegra Di2 FD-R8050',
//     'Altus RD-M2000',
//     'Altus FD-M313',
//     'XTR RD-M9000',
//     'Tourney RD-TX800',
//     'Alivio RD-T4000',
//     'Tourney RD-FT35',
//     'Ultegra FD-R8000',
//     'Tourney FD-TY300',
//     'Ultegra RX Di2 RD-RX805',
//     'Acera FD-M3000',
//     'Acera FD-T3000',
//     'Saint RD-M820',
//     'Deore RD-M592',

    'Altus FD-M310',
    'Altus RD-M310',
    'Dura Ace Di2 RD-R9250',
    'Ultegra FD-R8150',
    'Dura Ace FD-R9250',
    'Ultegra Di2 RD-R8150',
    'FD-M3120-M-B',
    'RD-M3100',

    'RD-M4120',
    'RD-M5100',
    'RD-M6100',
    'RD-M5120',
    'FD-M315-TS',
    'FD-TY710-2-TS3',
    'GRX RD-RX400',
    'GRX RD-RX812',
    'GRX RD-RX810',
    'GRX FD-RX810',
    'GRX FD-RX400',
    'SLX RD-M7100',
    'XT RD-M8100',
    'GRX Di2 RD-RX815',
    'GRX Di2 RD-RX817',
    'GRX Di2 FD-RX815',
  ],
  'mounts-sensors': [
    'Forerunner 920XT Wristband'
  ],
  'tacx-hydration': [
    'Deva',
    'Ciro',
    'Radar'
  ],
  'tacx-hydration': [
    'Deva',
    'Ciro',
    'Radar'
  ],
  'saddles': [
    'NOVUS Evo Boost Kit Carbonio Superflow',
    'NOVUS Evo Boost TM Superflow'
  ],
  'cockpit-small-parts': [
    // ridewrap
    'Chainstay Armor',
    'Shuttle Armor',
    'Essential Downtube Xtra Thick'
  ]
};

const tacxFilters = {
//   'tacx-trainer-bikes' : /(tacx)/i,
//   'tacx-trainer-smart' : /(tacx)/i,
   'tacx-trainer-wheelon' : /(tacx)/i,
  'tacx-trainer-rollers' : /(rollers)/i,
  'trainer-parts' : /(tacx).*?(feet|release|clamp)/i,
  'tacx-hydration' : /(deva|ciro|radar)/i,
  'tacx-trainer-wheelon' : /^(?!(feet))(boost)/i,
  'saddles' : /^(saddle|novus)/i,
  'components-derailleurs': /(^Red$|^Force22$|^X4$|^X3$|FD-TY710-2-TS3|GRX \w{2}-\w{2}\d{3}|GRX Di2 \w{2}-\w{2}\d{3}|\w{2}-\w\d{3}|XX1 Eagle AXS|Apex 1$|Altus FD-M310|EX1$|derailleur)(?!.*pulley.*)(?!.*kit)(?!.*parts)(?!.*hanger)/i,


  'cockpit-small-parts': /(chainstay armor|shuttle armor|Essential Downtube Xtra Thick)/i,

  'components-drivetrain-parts': /(ergopower cable set|cable guide|hanger|cam unit|routing plugs|adjuster kit|hanger bolt|cable ends|adjuster barrel|small parts|end capcable crimp|4mm sealed|4mm open|ferrule 4mm|housing stop|pivot screw|RoadLink|Goatlink|pulley|inner cage|cage lock|b-bolt|bolt kit|screw kit|parts kit|b-screw|spacer|anchor kit|limit screw|b-adjust|derailleur parts)/i,
  //(ergopower cable set|cable ends|end capcable crimp|4mm sealed|4mm open|ferrule 4mm|housing stop|ferrule|RoadLink)

  'mounts-sensors': /(wristband)/i,
  'tools': /(tire lever|chain whip|pliers|wrench|cleaner|cleaning|checker|keychain|brite)/i
};







const
  categories = {... tacxCats, ...shimano.categories, ...sram.categories },
  filters = {... tacxFilters, ...shimano.filters, ...sram.filters };

module.exports = { categories, filters };
