
const
  trek = require('./trek'),
  giant = require('./giant'),
  hlc = require('./hlc'),
  rocky = require('./rocky');
  specialized = require('./specialized'),
  thule = require('./thule');


const catalog = {
//  trek,
//  giant,
//  hlc,
  rocky,
  specialized,
  thule
};

Object.keys(catalog).forEach(c => {

  const
    filters = catalog[c].filters,
    categories = catalog[c].categories,
    positives = [],
    failures = [];

  Object.keys(filters).forEach(f => {
    Object.keys(categories).forEach(b => {
      const list = categories[b];

      list.forEach(i => {

        if (b === f) {

          if (!filters[f].test(i)) {
            failures.push('🚫 ' + c + ' ' + f + ': ' + i + ' _does not_ match ' + filters[f]);
          } else {
//            console.log(i, filters[f]);
          }
        } else {
          if (filters[f].test(i)) {
            positives.push('❗ ' + c + ' ' + f + ': ' + i + ' _does_ match ' + filters[f]);
          }
        }
      });
    });
  });

  const len = failures.length + positives.length;
  if (len) {

    console.log(len + ' error' + (len > 1 ? 's': '') + ':');
    let buf = '';
    if (failures) {
      failures.forEach(f => { buf += f + "\n"; });
    }
    if (positives) {
      positives.forEach(f => { buf += f + "\n"; });
    }
    console.log(buf);
  }

});
