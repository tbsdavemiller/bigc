const categories = {
  'frames': [
    'Slayer Carbon Frameset',
    'Altitude Carbon Frameset',
    'Instinct Carbon Frameset',
    'Element Carbon Frameset'
  ],
  'hardtail': [
    'Fusion 10',
    'Fusion 30',
    'Fusion 40',
    'Growler 50',
    'Growler 20',
    'Growler 40',
    'Soul 20',
    'Soul 10',
  ],
  'gravel': [
    'Solo 30',
    'Solo 50'
  ],
  'fat': [
    'Blizzard Alloy 10',
    'Blizzard Alloy 20',
    'Blizzard Carbon 30',
    'Blizzard Carbon 50'
  ],
  'dual': [
    'Altitude Carbon 70 Coil',
    'Altitude Carbon 90 Rally Edition',
    'Altitude Carbon 70',
    'Altitude Carbon 50',
    'Altitude Alloy 70 Coil',
    'Altitude Alloy 50',
    'Altitude Alloy 30',
    'Element Carbon 90',
    'Element Carbon 70',
    'Element Carbon 50',
    'Element Carbon 30',
    'Element Alloy 30',
    'Element Alloy 10',
    'Instinct Alloy 10',
    'Instinct Alloy 30',
    'Instinct Alloy 50',
    'Instinct Carbon 90',
    'Instinct Carbon 70',
    'Instinct Carbon 50',
    'Instinct Carbon 30',
    'Thunderbolt Ally 10',
    'Thunderbolt Ally 30',
    'Reaper 26',
    'Reaper 27.5',
  ],
  'ebike': [
    'Growler Powerplay 30',
    'Instinct Powerplay Carbon 90',
    'Instinct Powerplay Carbon 70',
    'Instinct Powerplay Alloy 70',
    'Instinct Powerplay Alloy 50',
    'Instinct Powerplay Alloy 30',
    'Altitude Powerplay Carbon 90 Rally Edition',
    'Altitude Powerplay Carbon 70',
    'Altitude Powerplay Alloy 70',
    'Altitude Powerplay Alloy 50',
  ],
  'kids-12': [
    'Edge Jr 12',
    'Edge Jr 14'
  ],
  'kids-16': [
    'Edge Jr 16',
  ],
  'kids-20': [
    'Soul Jr 20',
    'Vertex Jr 20',
    'Growler Jr 20',
  ],
  'kids-24': [
    'Growler Jr 24',
    'Growler Jr 26',
    'Edge Jr 24',
    'Edge Jr 26',
    'Soul Jr 24',
    'Vertex Jr 24',
    'Reaper 24',
  ]
};

const filters = {
  'frames' : /(frameset)/i,
  'dual': /^(?!.*(frameset|powerplay|24)).*(altitude|reaper|element|instinct|thunderbolt|slayer).*$/i,
  'ebike' : /(Powerplay)/i,
  'fat': /(blizzard)/i,
  'gravel': /(solo)/i,

  'kids-12': /(Edge Jr 12|Edge Jr 14)/i,
  'kids-16': /(Edge Jr 16)/i,
  'kids-20': /(Jr 20)/i,
  'kids-24': /(Jr 24|Jr 26|Reaper 24)/i,
  'hardtail': /^(?!.*(jr|powerplay)).*(fusion|growler|soul).*$/i,
};

module.exports = { categories, filters };
