const categories = {
  'tacx-trainer-bikes': [


  ],
  'tacx-trainer-smart': [

  ],
  'tacx-trainer-rollers': [
    'Tacx Antares rollers'
  ],
  'tacx-trainer-wheelon': [
    'Boost',
    'Boost Bundle',
  ],
  'tacx-trainer-mats': [

  ],
  'tacx-trainer-parts': [
    'Tacx, S0003, Rubber feet, Genius/Bushido/Satori/Boost',
    'Tacx, T1402, Quick release for rear wheel',
    'Tacx, T1902-03, Handlebar clamp',
    'Flux 2 resistance unit',
    'Rubber cap Flux S and Flux 2',
    'Resistance unit Flow smart',
    'Fitting kit Flow Smart',
//    ''
  ],
  'tacx-hydration': [
    'Deva',
    'Ciro',
    'Radar'
  ],
  'tacx-hydration': [
    'Deva',
    'Ciro',
    'Radar'
  ],
  'saddles': [
  ],
};

const filters = {
  'tacx-trainer-bikes' : /(neo bike)/i,
  'tacx-trainer-smart' : /(Flux 2|flow smart|NEO 2t smart|flux s smart)/i,
//  'tacx-trainer-wheelon' : /(boost)/i,
  'tacx-trainer-rollers' : /(rollers)/i,
  'tacx-trainer-mats' : /(trainer mat)/i,
  // /(tacx).*?(feet|release|clamp)/i,
  'tacx-trainer-parts' : /(NEO Trolley|quick release for rear wheel|axle Nut 3\/8"|axle nut M10|T1055|Grand Excel Frame|Front Fork Support|table handlebar mount|thru-axle for tacx trainer|towel|sweat cover|trainer bag|direct drive)/i,
  'tacx-hydration' : /(deva|ciro|radar)/i,
  'tacx-trainer-wheelon' : /^(?!(feet))(boost)/i,
  '//tacx-trainerparts': /(NEO Trolley|quick release for rear wheel|axle Nut 3\/8"|axle nut M10|T1055|Grand Excel Frame|Front Fork Support|table handlebar mount|thru-axle for tacx trainer|towel|sweat cover|trainer bag|direct drive)/i
};

module.exports = { categories, filters };
