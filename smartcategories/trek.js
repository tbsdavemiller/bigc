const categories = {
  'hardtail': [
    '820',
    'Procaliber 9.6',
    'X-Caliber 8',
    'X-Caliber 9',
    'Fusion 10',
    'Fusion 20',
    'Fusion 30',
    'Growler 50',
    'Marlin 8',
    'Soul 10',
    'Marlin 4',
    'Marlin 5',
    'Marlin 6',
    'Marlin 7',
    'Marlin 8',
    '820 Women\'s',
    'Roscoe 6',
    'Roscoe 7',
    'Roscoe 8',
    'Roscoe 9',
    'Procaliber 9.5',
    'Procaliber 9.6',
    'Procaliber 9.7',
  ],
  'kids-12': [
    'Kickster',
    'Precaliber 12',
    'Precaliber 12 Boys 12 Royal',
  ],
  'kids-16': [
    'Precaliber 16'
  ],
  'kids-20': [
    'Precaliber 20 7-speed',
    'Precaliber 20',
    'Roscoe 20',
  ],
  'kids-24': [
    'Precaliber 24 8-speed',
    'Precaliber 24 8SP Boys 24 Radioactive Red',
    'Precaliber 24 8-speed Suspension',
    'Roscoe 24',
    'Wahoo 24',
    'Wahoo 26'
  ],
  'mountain-frames': [
    'Farley EX Frameset',
    'Ticket DJ Frameset',
    'Ticket S Frameset',
    'Session 27.5 Frameset',
    'Slash C Frameset',
    'Session AL Frameset',
    'Fuel EX AL Frameset',
    'Fuel EX C Frameset'
  ],
  'hybrid-comfort': [
    'Verve 3 Disc Lowstep',
    'Verve 3 Disc',
    'Verve 2 Disc Lowstep',
    'Verve 2 Disc',
    'Verve 1 Disc Lowstep',
    'Verve 1 Disc'
  ],
  'road-frames': [
    'Émonda SLR RSL Frameset',
    'Émonda ALR Frameset',
    'Émonda ALR Disc Frameset',
    'Domane SLR Disc Frameset',
    'Émonda SLR Frameset',
    'Madone SLR Frameset',
    'Speed Concept SLR Frameset',
    'Boone Disc Frameset',
    'Speed Concept TT Frameset',
    'Checkpoint SL Frameset',
    '520 Disc Frameset',
    'Émonda SLR Disc Frameset'
  ],
  'dual-suspension': [

    'Fuel EX 9.8 XT',
    'Fuel EX 9.9 XTR',
    'Fuel EX 9.9 X01 AXS',
    'Fuel EX 9.7',
    'Fuel EX 7',
    'Fuel EX 8',
    'Fuel EX 5 Deore',
    'Fuel EX 9.8 GX',
    'Fuel EX 9.8 GX AXS',

    'Top Fuel 7 Deore/XT',
    'Top Fuel 5 Deore',
    'Top Fuel 8',
    'Top Fuel 9.7 SLX/XT',
    'Top Fuel 9.8 GX AXS',
    'Top Fuel 9.8 XT',
    'Top Fuel 9.8 GX',
    'Top Fuel 9.9 XTR',
    'Top Fuel 9.9 XX1 AXS',

    'Session 8 29 GX',
    'Session 9 X01',
    'Remedy 8',
    'Remedy 9.8',

    'Slash 8',
    'Slash 9.7 SLX/XT',
    'Slash 7',
    'Slash 9.8 GX AXS',
    'Slash 9.9 XX1 AXS',
    'Slash 9.9 XTR',
    'Slash 9.8 GX',
    'Slash 9.8 XT',
    'Slash 9.9 XX1 AXS Flight Attendant',

    'Supercaliber 9.7',
    'Supercaliber 9.9 XTR',
    'Supercaliber 9.9 XX1 AXS',
    'Supercaliber 9.8 XT',
    'Supercaliber 9.8 GX',
    'Supercaliber 9.7',
    'Supercaliber 9.6',
    'Supercaliber 9.8 GX AXS'
  ],
  'electric-road': [
    'Domane+ LT',
    'Domane+ ALR',
    'Domane+ LT 9',
  ],
  'electric-hybrid': [
    'Verve+ 2',
    'Verve+ 2 Lowstep',
    'Verve+ 3',
    'Verve+ 3 Lowstep',
    'Allant+ 7',
    'Allant+ 7 Lowstep',
  ],
  'electric-mountain': [
    'Powerfly FS 9 Equipped',
    'Powerfly FS 4',
    'Powerfly 4',
    'Rail 9.8 XT',
    'Rail 9.9 XTR',
    'Rail 9.9 XX1 AXS',
    'Rail 9.8 GX AXS',
    'Rail 9.8 GX',
    'Rail 5',
    'Rail 7',
    'Rail 9.7',
  ],
  'fat': [
    'Farley 5',
    'Farley 7',
    'Farley 9.6'
  ],
  'cx': [
    'Boone 6',
    'Crockett 5',
  ],
  'road': [
    '520 Grando',
    '520 Disc',
    'Speed Concept SLR 6 eTap',
    'Speed Concept SLR 7 eTap',
    'Speed Concept SLR 9 eTap',
    'Speed Concept SLR 9',
    'Speed Concept SLR 7',
    'Domane SLR 9',
    'Domane SLR 7 eTap',
    'Domane SL 7 eTap',
    'Domane AL 2',
    'Madone SLR 9',
    'Madone SL 7',
    'Madone SL 7 eTap',
    'Madone SLR 7 eTap',
    'Madone SLR 9 eTap',
    'Domane SLR 7',
    'Madone SLR 7',
    'Domane SL 5',
    'Domane SL 6',
    'Domane SLR 6 eTap',
    'Madone SLR 6 eTap',
    'Domane SL 6 eTap',
    'Domane SLR 9 eTap',
    'Domane AL 3 Disc',
    'Domane AL 2 Disc',
    'Domane AL 4 Disc',
    'Domane AL 5 Disc',
    'Domane SL 7'
  ],
  'gravel': [
    'Checkpoint SLR 6 eTap',
    'Checkpoint SLR 7',
    'Checkpoint SLR 7 eTap',
    'Checkpoint SLR 9 eTap',
    'Checkpoint SL 5',
    'Checkpoint SL 6 eTap',
    'Checkpoint SL 7 eTap',
    'Checkpoint ALR 5',
  ],

  'drivetrain-parts': [
    'Madone Chainstay Rubber Gasket'
  ],

  'seatposts-levers': [
    'Madone 9 Aero Micro-adjust Seatpost',
  ],

  'noise': [
    'Cable Management Trek Fuel EX Carbon BB Guide 2020 Black',
    'Cable Management Trek Top Fuel 29 2020 Alloy Lockout Grommet',
    'Cable Management Trek Top Fuel 29 2020 Carb Lockout Grommet',

    // chain stuff

    'Bontrager Chain Lube',
    'Bontrager Long Chainstay Protector',
    'Bontrager Universal Chainstay Protector',
    'Bosch Gen 3 Narrow Wide Chainring',
//    'Chainguard - Dutchi Kids 16" - Red',
  ],
  'cockpit-small-parts': [
    'Trek Checkpoint Carbon Armor',
    'Trek 2016 Farley 27.5 Carbon Downtube Guard',
    'Armor RideWrap Trek Top Fuel Covered Kit Clear Matte',
    'Checkpoint Control Center Covers',
    'Checkpoint SL IsoSpeed Covers',
    'Bontrager Madone DuoTrap Chainstay Frame Cover',
    'Armor Trek Fuel EX 2020 Carbon Downtube Guard Black',
    'Armor Trek Fuel EX 2020 Carbon Seatstay Guard Black',
    'Armor Trek Fuel EX 2020 Chainstay Guard Black',
    'Armor Trek Fuel EX 2021 Aluminum Chainstay Guard Kit Black',
    'Armor Trek Slash Carbon Lower Downtube 2021',
    'Armor Trek Supercaliber & Top Fuel 2020 Downtube Guard Clear',
    'Armor Trek Top Fuel 29 2020 Aluminum Chainstay Guard Black',
    'Armor Trek Top Fuel 29 2020 Carbon Chainstay Guard Black',
    'Armor Trek Top Fuel 29 2020 Rotor Strike Guard',
    'Armor Trek Top Fuel 29 2022 Aluminum Chainstay Black',
    'Armor Trek Top Fuel 29 2022 Carbon Chainstay Black',
    'Armor Trek Top Fuel 29 2022 Upper Downtube Black',
    'Fisher 2013 Superfly SL 29 Chainstay Guard',
    'Trek 2016 Farley 27.5 Carbon Downtube Guard'
  ],
  'helmet-parts': [
    'Bontrager Blendr Lithos MIPS Bike Helmet Mount',
    'Bontrager Specter WaveCel Bike Helmet Fit Pad',
    'Bontrager Solstice Bike Helmet Visor',
    'Bontrager Quantum MIPS Bike Helmet Visor',
    'Bontrager Solstice MIPS Bike Helmet Fit Pad',
    'Bontrager XXX WaveCel NoSweat Bike Helmet Fit Pad',
    'Bontrager Starvos WaveCel Bike Helmet Fit Pad',
    'Bontrager XXX WaveCel NoSweat Bike Helmet Fit Pad',
    'Bontrager Ballista MIPS Bike Helmet Fit Pad',
    'Bontrager Quantum MIPS Bike Helmet Visor',
    'Bontrager Velocis MIPS Bike Helmet Fit Pad',
    'Bontrager Rally MIPS Bike Helmet Pad Kit'

  ],
  'helmets-eyewear-mountain': [
    'Bontrager Rally WaveCel Mountain Bike Helmet',
    'Bontrager Solstice Bike Helmet',
    'Bontrager Solstice MIPS Bike Helmet',
    'Bontrager Blaze WaveCel LTD Mountain Bike Helmet',
    'Bontrager Specter WaveCel Cycling Helmet',
    'Bontrager Blaze WaveCel Mountain Bike Helmet',
    '100% TRAJECTA Helmet',
  ],
  'helmets-eyewear-road': [
    'Electra Miami Lifestyle Helmet',
    'Electra Tutti Frutti Lifestyle Helmet',
    'Electra Unicorn Lifestyle Bike Helmet',
    'Electra Lifestyle Lux Cool Cat Helmet',
    'Electra EBC 3000 Lifestyle Bike Helmet',
    'Bontrager Starvos WaveCel Round Fit Helmet',
    'Bontrager Quantum MIPS Bike Helmet',
//    'Bontrager Solstice MIPS Bike Helmet',
    'Bontrager Charge WaveCel Commuter Helmet',
    'Bontrager XXX WaveCel LTD Road Bike Helmet',
    'Bontrager Quantum MIPS Bike Helmet'
  ],
  'locks': [
    'Bontrager Comp Keyed Chain Lock',
    'Bontrager Elite Combo Chain Lock',
    'Bontrager Elite Keyed Chain Lock',
    'Bontrager Kids Combo Chain Lock',
    'Electra ABUS Steel Chain Combo Lock'
  ],
  'bike-storage': [

  ],
//   'pedals': [
//     'Bontrager City Pedal Set',
//     'Bontrager Elite Road Pedal Set',
//     'Bontrager Line Pro MTB Pedal Set',
//     'Bontrager Commuter Pedal Set',
//     'Bontrager Line Comp Flat Pedal Set',
//     'Bontrager Line Elite MTB Pedal Set',
//     'Bontrager Comp MTB Pedal Set',
//     'Bontrager Elite Road Pedal Set',
//   ],
//   '': [
//
//   ],
  'tools': [
    'KMC Portable Mini Chain Tool',
    'Park Tool CC-4 Chain Checker',
    'Park Tool CC-3.2 Chain Wear Checker',
    'Park Tool CG-2.4 Chain Gang Chain Cleaning System',
//    'Park Tool CM-5.3 Cyclone Chain Cleaner'
  ],
  'chains': [
    'KMC K1SL SuperLite 1/8" Chain',
    'KMC K1SL SuperLite Nickel Plated 1/8" Chain',
    'KMC Missing Link DLC Reusable Shimano Chain Link',
    'KMC Missing Link DLC Reusable Chain Link',
    'KMC, K1 Narrow, Chain, Speed: 1, 3/32", Links: 112, Silver'
  ],
  'trainer-parts': [
    'Saris Smart Trainer USB Adapter',
    'Saris Smart Trainer ANT+ USB Micro Adapter',
  ],
};

const filters = {

  // /(session 27.5 frameset|session al frameset|slash c frameset|ticket dj frameset|ticket s frameset)/i
  'mountain-frames': /^(slash|fuel|ticket|session|farley).*?(frameset)$/i,

  // (disc frameset|alr frameset|sl frameset|slr frameset|tt frameset)
  'road-frames': /^(madone|domane|Émonda|speed concept|boone|checkpoint|520).*?(frameset)$/i,

  'road': /^(madone|domane|520|speed concept|Émonda)((?!frameset|gasket|seatpost|\+).)*$/i,

  // ^(?!(armor|cable))(820|marlin|procaliber|fusion|growler|soul|roscoe|x-caliber)
  'hardtail': /^(?!(armor|cable))(820|marlin|procaliber|fusion|growler|soul|roscoe \d{1}\b|x-caliber)/i,

  'fat': /^(?!(guard))(farley)((?!frameset|\+).)*$/i,

  // ^(?!(armor|cable|pad|padset|frame|part))(fuel ex|remedy|session 8|session 9|slash|supercaliber 9|top fuel)
  'dual-suspension': /^(?!(guard))(fuel ex|remedy|instinct|altitude|session|slash|supercaliber|top fuel)((?!frameset).)*$/i,

  // ^(?!(frameset))(checkpoint sl 5|checkpoint sl 6|checkpoint sl 7|checkpoint slr 6|checkpoint slr 7|checkpoint slr 9|520 disc|520 grando|checkpoint alr 4|checkpoint alr 5)
  'gravel': /^(checkpoint)((?!frameset|cover).)*$/i,

  // ^(?!(frameset))(boone 6|crockett 5)
  'cx': /^(?!(frameset))(boone \d{1}|crockett)/i,

  'electric-road': /(domane\+)/i,

  'electric-mountain': /^(?!(armor|padset))(e-\cal|powerfly|rail)/i,

  'electric-hybrid': /^(?!(rack))(ace of spades go!|bali go!|cruiser go!|ghostrider go!|navigator go!|shibori go!|townie path go!|townie go!|vale go!|verve\+|allant\+)/i,

  'hybrid-comfort': /^(?!(rack|fender|bell|plate))(townie original 7d|townie path 9d|loft 7d|loft 7i|verve 1|verve 2|verve 3)/i,

  'hybrid-fitness' : /^(?!(rove e+|thrive e+))(district 4|dual sport 1|dual sport 2|dual sport 3|dual sport 4|fx 1|fx 2|fx 3|fx 4|fx sport)/i,

  'kids-20': /(jungle 1 20|firetail 1 20|precaliber 20|roscoe 20|sprocket 1 20"|wahoo 20)/i,

  'kids-24': /(precaliber 24|roscoe 24|wahoo 24|wahoo 26)/i,

  'cockpit-small-parts': /(armor|guard|cover)/i,

  'seatposts-levers': /(seatpost)$/i,
//  'seatpost-parts': /(seatpost)/i,

  'helmet-parts': /(Helmet Part|Helmet pad Kit|Segafredo decal|helmet fit|helmet visor|blendr rally mips|blendr circuit mips|helmet buckle|helmet strap|blendr lithos|helmet vent cover)/i,

  'helmets-eyewear-mountain': /^(?!.*(pad|visor)).*?(rally|solstice|blaze|specter|trajecta).*?/i,

  'helmets-eyewear-road': /^(?!.*(pad|visor|mount)).*(Lifestyle Bike Helmet|quantum|lifestyle lux bike helmet|lifestyle helmet|lifestyle lux|starvos wavecel|xxx wavecel|road bike helmet|commuter helmet).*$/i,
  // /^(?!.*(pad|visor|mount)).*(Lifestyle Bike Helmet|MIPS Bike Helmet|lifestyle lux bike helmet|lifestyle helmet|lifestyle lux|starvos wavecel|xxx wavecel|road bike helmet|commuter helmet).*$/i,

  'locks': /(lock(?!o))/i, // lock, but not lockout

  'chains': /^(?!(.*?abus|checker|combo|armor|key|stay|lock|tool|lube|guard)).*?(chain|link)(?!(abus|ring|combo|armor|key|stay| lock| tool| lube| checker|guard))/i,

  'trainer-smart': /(smart trainer)(?!.*adapter)/i,

  'trainer-wheel-on': /(Saris Mag|Fluid2|Mag trainer|drive trainer)/i,

  'trainer-mats': /(floor mat|sweat guard|trainer mat)/i,

  'trainer-parts': /(Trainer Part|thru axle adapter|trainer skewer|trainer usb adapter|trainer ANT+)/i,


};

module.exports = { categories, filters };

