const categories = {
  'hardtail': [
    'Tempt 0',
    'Tempt 1',
    'Tempt 2',
    'Tempt 3',
    'Bliss',
    'Talon',
    'Talon 1',
    'Talon 2',
    'ATX',
    'Fathom 1',
    'Fathom 2',
    'Fathom 29 1',
    'Fathom 29 2',
    'XTC SLR 29',
    'XTC Advanced 29',
//    'STP 26 SS'
  ],
  'dual-suspension': [
    'Anthem Advanced Pro 29',
    'Trance',
    'Trance X',
    'Trance X Advanced Pro 29',
    'Trance X 29',
    'Trance Advanced Pro 29',
    'Trance 29 1',
    'Trance 29 2',
    'Stance 29',
    'Stance',
    'Reign 29',
    'Reign 29 SX',
    'Reign Advanced Pro 29',
  ],
  'hybrid-comfort': [
    'Sedona DX',
    'Sedona DX W',
    'Cypress DX',
    'Cypress 1',
    'Cypress 2',
    'Cypress 3',
  ],
  'hybrid-fitness': [
    'Thrive',
    'Alight',
    'Alight Disc',
    'Alight 2 DD City Disc',
    'Alight 1 DD Disc',
    'Escape 3',
    'Fastroad SL1',
    'Alight DD Disc 1',
    'Roam 4 Disc',
    'Roam Disc 0',
    'Roam Disc 1',
    'Roam Disc 2',
    'Roam Disc 2 (GU)',
    'Roam Disc 3',
    'Roam Disc 4',
    'Rove 1 DD',
    'Rove 2 DD',
    'Rove 3 DD (GU)',
    'Rove 3 DD',
    'Rove 4 DD',
    'Escape 2 Disc'
  ],
  'helmets-eyewear-road': [
    'Rev',
    'Rev MIPS',
    'Rev Pro MIPS',
    'Rev Comp MIPS',
    'Rev Comp',
    'Relay MIPS',
  ],
  'fenders': [
    'Revolt and Devote Fender',
  ],
  'hybrid-cargo': [
    'Pakyak'
  ],
  'electric-cargo': [
    'Pakyak E+'
  ],
  'gravel': [
    'Revolt 0',
    'Revolt 1',
    'Revolt Advanced 0',
    'Revolt Advanced 1',
    'Revolt Advanced 2',
    'Revolt Advanced 3',
    'Revolt Advanced Pro 0',
    'Revolt Advanced Pro 1',
    'Devote 1',
    'Devote 2',
    'Devote Advanced 1',
    'Devote Advanced 2',
    'Devote Advanced Pro',
    'ToughRoad SLR 1',
    'ToughRoad SLR 2',
  ],
  'computers': [
    'Neos GPS'
  ],
  'cockpit-parks': [
    'Aero Clip On Clamp Shim for Enviliv Handlebar'
  ],
  'mounts-sensors': [
    'Giant Heart Rate Sensor',
    'Giant Speed & Cadence Magnet Set For RideSense 2.0',
    'Giant ANT+ & BLE 2 in 1 Speed & Cadence Sensor',
    'Giant RideSense 2.0 (ANT+/BLE)',
    'Contact SLR/SL Stem mount',
    'Adjustable GoPro Mount',
    'Contact SLR Aero Garmin & GoPro Mount',
    'MTB Mount With Giant & Garmin Tray',
    'Round Handlebar Garmin & GoPro Mount',
    'NeosTrack TT Extension Mount - Team Edition',
    'GoPro Computer Mount',
    'Garmin Mount Tray for Computer/GoPro Mount'
  ],
  'road': [
    'Contend 3',
    'Contend AR 1',
    'Contend AR 2',
    'Contend AR 3',
    'Contend AR 4',
    'Defy Advanced 2',
    'Defy Advanced 3',
    'Defy Advanced Pro 1',
    'Defy Advanced Pro 2 Ultegra',
    'Defy Advanced Pro 3',
    'Propel Advanced Disc 1',
    'Propel Advanced Pro Disc 0',
    'Propel Advanced SL Disc 1',
    'TCR Advanced SL Disc 0',
    'TCR Advanced SL Disc 1',
    'TCR Advanced 1 Disc',
    'TCR Advanced 2 Disc',
    'Trinity Advanced Pro 1',
    'TCR Advanced SL Disc 0 RED',
    'TCR Advanced SL Disc 0 Dura-Ace'
  ],
  'electric-road': [
    'Road E+ 1 Pro',
    'Revolt E+',
  ],
  'electric-hybrid': [
    'Amiti E+',
    'Rove E+',
    'Explore E+ 1 Pro GTS',
    'Explore E+ 1 Pro STA',
    'Explore E+ 2 GTS',
    'Roam E+ GTS',
    'Thrive E+ EX Pro',
    'FastRoad E+ EX',
    'Explore E+ 2 STA',
    'Roam E+ STA'
  ],
  'electric-mountain': [
    'Intrigue X E+ 1',
    'Intrigue X E+ 2',
    'Tempt E+ 2',
    'Embolden E+ 2',
    'Vall E+ Pro',
    'Tempt E+',
    'Intrigue X E+',
    'Talon E+ 2',
    'Talon E+ 2 29',
    'Reign E+ 1',
    'Reign E+ 2',
    'Trance X Advanced E+ 2',
    'Trance X E+ 1',
    'Trance X E+ 3',
    'Fathom E+ 1 Pro',
  ],
  'fat': [
    'Yukon 1',
    'Yukon 2',
  ],
  'cx': [
    'TCX Advanced Pro 0',
    'TCX Advanced Pro 1',
  ],
  'kids-12': [
    'Pre',
  ],
  'kids-16': [
    'Animator F/W 16',
    'Animator 16',
  ],
  'kids-20': [
    'XtC Jr 20',
    'XTC Jr 20 Lite',
    'XtC Jr 20',
    'STP 20 FS',
    'STP 20',
    'ARX 20'
  ],
  'kids-24': [
    'ARX 24',
    'STP 24',
    'STP 26',
    'Trance Jr 26',
    'XtC Jr Disc 24',
    'XTC Jr 24 Lite',
    'XTC SL Jr 24',
    'XTC Jr 24 Lite',
    'XTC Jr Disc 24'
  ]
};

const filters = {
  'hardtail' : /^(?!(fathom e+|tempt e+|talon e+|stp 20|stp 24|stp 26))(stp|xtc slr|talon 0|talon 1|talon 2|talon 3|xtc advanced|fathom 1|fathom 2|fathom 29|talon|fathom|atx|tempt|tempt 0|tempt 1|tempt 2|tempt 3|bliss)/i,
  'dual-suspension' : /^(?!(trance jr|trance x e|trance x advanced e|embolden e|intrigue x e|stance e|reign e))(reign|anthem|trance|trance 29|trance advanced|trance x|stance 29|intrigue|pique|embolden|stance)/i,
  'hybrid-fitness' : /^(?!(rove e+|thrive e+))(escape 2|escape 3|escape city|escape disc|fastroad sl|fastroad advanced|vida low|roam \d disc|roam disc \d|rove|thrive|alight)/i,
  'road': /^(?!(road e\+))(contend 3|contend ar|defy advanced|propel advanced|tcr advanced|trinity)/i,
  'electric-road': /(revolt e+|road e\+ 1 pro)/i,
  'electric-hybrid': /(amite e+|rove e+|fastroad e+|explore e+|transcend e+|vida e+|lafree e+|amiti e+|roam e+|thrive e+|intrigue e+|vall e+ pro)/i,
  'electric-mountain': /^(?!(roam e+))(trance x advanced e+|trance x e+|fathom e+|reign e+|fathom e+|stance e+|talon e+|roam e+|embolden e+|tempt e+|intrigue e+|vall e+|intrigue x e+)/i,
  'fat': /^(yukon)/i,
  'gravel': /^(?!(fender))(toughroad|revolt 0|revolt 1|revolt 2|revolt advanced|devote)/i,
  'hybrid-comfort': /^(sedona|cypress|flourish)/i,
  'electric-cargo': /^(pakyak e)/i,
  'hybrid-cargo': /^(?!(pakyak e+))(pakAway|pakyak)/i,
  'kids-12': /(\bpre\b)/i,
  'kids-16': /(animator)/i,
  'kids-20': /(arx 20|stp 20|xtc jr 20|xtc jr 20)/i,
  'kids-24': /(xtc jr disc 24|stp 24|stp 26|xtc jr 24|arx 24|xtc sl jr 24|trance jr)/i,

  'fenders': /(fender)/i,
  'helmets-eyewear-road': /(rev\b|relay\b|Extima|Infinita sx|luta)/i,

//  'helmets-parts':

//  n

  'mounts-sensors': /(Mount|Heart Rate Sensor|Magnet Set|cadence sensor|RideSense)/i
};


module.exports = { categories, filters };
