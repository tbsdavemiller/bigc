const categories = {
  'electric-hybrid': [
    'Turbo Vado SL 5.0 Step-Through EQ Turbo Vado SL Specialized',
    'Turbo Vado SL 4.0 Step-Through EQ Turbo Vado SL Specialized',
    'Turbo Vado SL 5.0 EQ Turbo Vado SL Specialized',
    'Turbo Como 3.0 Turbo Como  Specialized',
    'Turbo Como 3.0 IGH Turbo Como  Specialized',
    'Turbo Como 4.0 Turbo Como  Specialized',
    'Turbo Como 4.0 IGH Turbo Como  Specialized',
    'Turbo Como 5.0 IGH Turbo Como  Specialized',
    'Tarmac SL6 Sport Tarmac SL6  Specialized',
    'Tarmac SL7 Comp - Rival eTap AXS Tarmac SL7  Specialized',

    'Turbo Vado 3.0 IGH Turbo Vado  Specialized',
    'Turbo Vado 3.0 IGH Step-Through Turbo Vado  Specialized',
    'Turbo Vado 5.0 IGH Turbo Vado  Specialized',
    'Turbo Vado 5.0 IGH Step-Through  Turbo Vado  Specialized',
    'Turbo Vado 3.0 Turbo Vado  Specialized',
    'Turbo Vado 3.0 Step-Through Turbo Vado  Specialized',
    'Turbo Vado 4.0 Turbo Vado  Specialized',
    'Turbo Vado 4.0 Step-Through Turbo Vado  Specialized',
    'Turbo Vado 5.0 Turbo Vado  Specialized',
    'Turbo Vado 5.0 Step-Through Turbo Vado  Specialized',
  ],
  'hybrid-comfort': [
    'Roll 2.0 Roll  Specialized',
    'Roll 2.0 Low Entry Roll  Specialized',
    'Roll 3.0 Roll  Specialized',
    'Roll 3.0 Low Entry Roll  Specialized',
  ],
  'bikes-gravel': [
    'Diverge Comp Carbon Diverge  Specialized',
    'Diverge E5 Diverge  Specialized',
    'Diverge Comp E5 Diverge  Specialized',
    'Diverge Elite E5 Diverge  Specialized',
    'Diverge Expert E5 EVO Diverge EVO  Specialized',
    'Diverge Expert Carbon Diverge  Specialized',
    'Diverge Pro Carbon Diverge  Specialized',
    'Diverge Sport Carbon Diverge  Specialized',
    'Diverge Diverge  Specialized'
  ],
  'bikes-road': [
    'Tarmac SL6',
    'Tarmac SL7 Expert Tarmac SL7  Specialized',
    'Tarmac SL7 Comp - Rival eTap AXS Tarmac SL7  Specialized',
    'Tarmac SL7 Pro - SRAM Force eTap AXS Tarmac SL7  Specialized',
    'S-Works Tarmac SL7 - Shimano Dura-Ace Di2 Tarmac SL7  Specialized',
    'S-Works Tarmac SL7 - SRAM Red eTap AXS Tarmac SL7  Specialized',
    'Allez Sport Allez  Specialized',
    'Allez Elite Allez  Specialized',
    'Aethos Comp - Rival eTap AXS Aethos  Specialized',
    'Aethos Expert Aethos  Specialized',
    'Aethos Pro - SRAM Force eTap AXS Aethos  Specialized',
    'S-Works Aethos - Dura-Ace Di2 Aethos  Specialized',
    'S-Works Aethos - SRAM Red eTap AXS Aethos  Specialized',
    'Roubaix Comp - SRAM Rival eTap AXS Roubaix  Specialized'
  ],
  'mountain-dual': [
    'Epic Comp Epic  Specialized',
    'Epic Expert Epic  Specialized',
    'Epic Pro Epic  Specialized',
    'S-Works Epic Epic  Specialized',
    'Stumpjumper EVO Comp Stumpjumper EVO  Specialized',
    'Stumpjumper EVO Comp Alloy Stumpjumper EVO  Specialized',
    'Stumpjumper EVO Elite Alloy Stumpjumper EVO  Specialized',
    'Stumpjumper EVO Expert Stumpjumper EVO  Specialized',
    'Stumpjumper EVO Pro Stumpjumper EVO  Specialized',
    'S-Works Stumpjumper EVO Stumpjumper EVO  Specialized',
    'Epic EVO Epic EVO  Specialized',
    'Epic EVO Comp Epic EVO  Specialized',
    'Epic EVO Expert Epic EVO  Specialized',
    'Epic EVO Pro Epic EVO  Specialized',
    'S-Works Epic EVO Epic EVO  Specialized',

  ],
  'mountain-hardtail': [
    'Epic Hardtail Epic Hardtail  Specialized',
    'Epic Hardtail Comp Epic Hardtail  Specialized',
    'Rockhopper 26 Rockhopper  Specialized',
    'Rockhopper 27.5 Rockhopper  Specialized',
    'Rockhopper 29 Rockhopper  Specialized',
    'Rockhopper Elite 27.5 Rockhopper  Specialized',
    'Rockhopper Elite 29 Rockhopper  Specialized',
    'Rockhopper Expert 29 Rockhopper  Specialized',
    'Rockhopper Sport 26 Rockhopper  Specialized',
    'Rockhopper Sport 27.5 Rockhopper  Specialized',
    'Rockhopper Sport 29 Rockhopper  Specialized',
    'Chisel Chisel Specialized',
    'Chisel Comp Chisel  Specialized',
    'Rockhopper Expert 27.5 Rockhopper  Specialized',
    'Fuse 27.5 Fuse  Specialized',
    'Fuse Comp 29 Fuse  Specialized',
    'Fuse Expert 29 Fuse  Specialized',
    'Fuse Sport 27.5 Fuse  Specialized',
  ],
  'mountain-frames': [
    'Enduro Frameset Enduro Specialized',
    'Stumpjumper EVO Alloy Frameset',
    'Stumpjumper EVO Frameset',
     'S-Works Turbo Levo Frameset Turbo Levo  Specialized'
  ],
  'cx': [
    'Crux Comp Crux  Specialized',
    'Crux Expert  Crux  Specialized',
    'Crux Pro  Crux  Specialized',
    'S-Works Crux Crux  Specialized',
  ],
  'road-frames': [
//     'S-Works Tarmac SL7 Frameset Tarmac SL7  Specialized',
//     'S-Works Aethos Frameset Aethos  Specialized',
    'S-Works Tarmac SL7 Frameset',
    'S-Works Aethos Frameset'
  ],
  'hybrid-fitness': [
    'Sirrus 1.0 Sirrus  Specialized',
    'Sirrus 2.0 Step-Through Sirrus  Specialized',
    'Sirrus 2.0 Sirrus  Specialized',
    'Sirrus 3.0 Sirrus  Specialize',
    'Sirrus 4.0 Sirrus  Specialized',
    'Sirrus 6.0 Sirrus  Specialized',
  ],
  'electric-road': [
    'Turbo Creo SL Comp Carbon Turbo Creo  Specialized',
    'Turbo Creo SL Comp E5 Turbo Creo  Specialized',
    'Turbo Creo SL Expert Turbo Creo  Specialized',
    'Turbo Creo SL Expert EVO Turbo Creo,Turbo Creo EVO  Specialized',
    'S-Works Turbo Creo SL Turbo Creo  Specialized',
    'S-Works Turbo Creo SL EVO Turbo Creo,Turbo Creo EVO  Specialized',
  ],
  'electric-mountain': [
    'Turbo Tero 3.0 Turbo Tero  Specialized',
    'Turbo Tero 3.0 Step-Through Turbo Tero  Specialized',
    'Turbo Tero 4.0 Turbo Tero  Specialized',
    'Turbo Tero 5.0 Turbo Tero  Specialized',

    'Turbo Levo SL  Specialized',
    'Turbo Levo Expert Turbo Levo  Specialized',
    'Turbo Levo Comp Turbo Levo  Specialized',
    'Turbo Levo Pro Turbo Levo  Specialized',
    'S-Works Turbo Levo Turbo Levo  Specialized',
    'Turbo Levo Alloy Turbo Levo  Specialized',
    'Turbo Levo Comp Alloy Turbo Levo  Specialized',

    'Turbo Kenevo SL Comp',
    'Turbo Kenevo SL Expert',
    'S-Works Turbo Kenevo SL',
//    'Turbo Kenevo SL Expert Kenevo SL  Specialized',
//    'S-Works Turbo Kenevo SL Kenevo SL  Specialized',
//    'Kenevo Comp Turbo Kenevo  Specialized',

  ],
  'electric-hybrid': [
    'Turbo Como SL 4.0 Turbo Como SL  Specialized',
    'Turbo Como SL 5.0 Turbo Como SL  Specialized',
  ],
  'kids-12': [
    'Riprock Coaster 12'
  ],
  'kids-16': [
    'Jett 16',
    'Riprock Coaster 16',
  ],
  'kids-20': [
    'Jett 20',
    'Riprock 20',
    'Riprock Coaster 20',
  ],
  'kids-24': [
    'Jett 24',
    'Riprock 24 Riprock  Specialized',
    'Riprock Expert 24 Riprock  Specialized',
    'Jett 24',
  ],
  'grips': [
    'Supacaz Siliconez Grip'
  ],
  'kickstands': [
    'Roll Kickstand',
  ],
  'baskets-racks': [
    'Roll/Como Rack',
    'Roll Mini Basket '
  ],
  'fenders': [
    'Roll Fender Set'
  ],
  'bottles-cages': [
    'Supacaz Side Swipe Cage Poly – Left',
    'Roll Cage Roll Cage SWAT™ Specialized'
  ],
  'tires': [
    'Roller Specialized',
  ],
  'wheels': [
    'Stout XC SL 29 Front',
  ],
  'tubeless-parts': [
    'Roval Traverse Tubeless Valves',
    'Roval Tubeless Valves',
    'Roval Control SL Tubeless Valves'
  ]
};


const filters = {
  'bottles-cages': /(roll cage|swipe cage)/i,
  'wheels': /(stout)/i,
  'grips': /(grip)/i,

  'electric-hybrid': /(vado|como)(?!.*rack)/i,
  'electric-mountain': /(?<!frame.*)(?:tero|levo|kenevo)(?!.*frame)/i,
  'electric-road': /(creo)/i,

  'mountain-hardtail': /(epic.*?hardtail|fuse|rockhopper|chisel)/i,
  'mountain-dual': /^(?!.*(hardtail|frameset)).*?(enduro|epic|stumpjumper).*?/i,

  'bikes-gravel': /(diverge)/i,
  'cx': /(crux)/i,

  'bikes-road': /(?<!frame.*)(?:tarmac|aethos|roubaix|allez)(?!.*frame)/i,
  'hybrid-fitness': /(sirrus)/i,
  'hybrid-comfort': /(roll \d{1}.0)/i,

  'road-frames': /(tarmac|aethos).*?(frame)/i,
  'mountain-frames': /(enduro|stumpjumper|levo).*?(frame)/i,

  'kids-12': /(riprock coaster 12)/i,
  'kids-16': /(jett 16|riprock.*?16)/i,
  'kids-20': /(jett.*?20|riprock.*?20)/i,
  'kids-24': /(jett.*?24|riprock.*?24)/i,

  'baskets-racks': /(rack|basket)/i,
  'fenders': /(fender)/i,
  'kickstands': /(kickstand)/i,

  'tubeless-parts': /(tubeless valve)/i,
//  'tires': /(roller)/i,
};

module.exports = { categories, filters };

