const categories = {
  'bike-gear-bags': [
    'RoundTrip bike duffel',
    'Roundtrip Road Bike Travel Case',
    'Roundtrip Mtb Travel Case',
    'Roundtrip Case Rack Mounting Kit',
    'Thule Rail Hip Pack',
    'Rail Backpack'
  ],
  'fork-mount-parts': [
    'Thule Low Rider',
    'Thule Locking Low Rider',
  ],
  'tailgate-pads': [
    'Thule GateMate PRO truck bed bike rack black',
  ],
  'trailers-accessories': [
    'Thule Bottle Cage',
    'Thule organizer sport double black',
    'Thule organizer sport black',
    'Thule Footmuff Sport',
    'Thule Sapling baby backpack black',
    'Thule Sapling baby backpack agave',
    'Thule Sapling Rain Cover'
  ],
  'baskets-racks': [
    'Thule Pack \'n Pedal Rail Extender Kit'
  ],
  'hydration-packs': [
    'Rail Pro Hydration Pack',
    'Rail Hydration Pack 8L',
    'Thule UpTake hydration pack',
    'Thule Vital hydration pack'
//    'Rail Hip Pack 2L',
  ]
};


const filters = {
  'bike-gear-bags': /^(?!.*(hydration|extender)).*(roundtrip|rail)/i,
  'fork-mount-parts': /(low rider)/i,
  'tailgate-pads': /(gatemate)/i,
  'trailers-accessories': /(bottle cage|organizer sport|footmuff|baby backpack| sapling rain cover)/i,
  'hydration-packs': /(hydration pack)/i
};

module.exports = { categories, filters };

