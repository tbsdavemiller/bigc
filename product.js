const
  config = require('./config'),
  requestPromise = require('request-promise');

const product = (product, options) => {

  if (! options || ! options.env) {
    return new Promise((resolve, reject) => {
      reject('No env provided');
    });
  }

  const env = config.env[options.env];
  if (! env) {
    return new Promise((resolve, reject) => {
      reject(`Invalid env ${key}`);
    });
  }

  const key = options.env;

  const createURL = (product) => {

    const obj = {
      url: `${config.api.domain}/${env.id}/v3/catalog/products`,
      json: true,
      body: product,
      method: 'post',
      headers: config.api.headers(key)
    };

    return obj;
  };

  const retrieveURL = (q, page) => {

    const url = `${config.api.domain}/${env.id}/v3/catalog/products/?keyword=${q}${page ? '&page=' + page : ''}`;
    return {
      url: url,
      json: true,
      method: 'get',
      headers: config.api.headers(key)
    };
  };

  const updateURL = (product) => {

    return {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}`,
      json: true,
      body: product,
      method: 'put',
      headers: config.api.headers(key)
    };
  };

  const customFieldsURL = (product) => {
    return {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/custom-fields`,
      json: true,
      body: product.custom,
      method: 'POST',
      headers: config.api.headers(key)
    };
  };

  const optionsURL = (product) => {
    return {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/variants`,
      json: true,
      method: 'GET',
      headers: config.api.headers(key)
    };
  };

  const productMetaURL = (product, data ) => {
    const obj = {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/metafields`,
      json: true,
      method: 'POST',
      body: data,
      headers: config.api.headers(key)
    };
//    console.log(obj);
    return obj;
  };

  const variantMetaURL = (product, variant, data ) => {
    const obj = {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/variants/${variant.id}/metafields`,
      json: true,
      method: 'POST',
      body: data,
      headers: config.api.headers(key)
    };
//    console.log(obj);
    return obj;
  };

  const defaultOptionURL = (product, option, id) => {

    const url = `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/options/${option.id}/values/${id}`;
    return {
      url: url,
      json: true,
      body: { is_default: true },
      method: 'PUT',
      headers: config.api.headers(key)
    };
  };

  const imageURL = (product, index) => {

    return {
      url: `${config.api.domain}/${env.id}/v3/catalog/products/${product.id}/images`,
      json: true,
      method: 'POST',
      body: product.photos[index],
      headers: config.api.headers(key)
    };
  };

  let rows = [];

  if (product.id) {
    return requestPromise(updateURL(product));
  } else if (product.upc || product.sku) {
    return requestPromise(retrieveURL(product.upc ? product.upc : product.sku))
    .then(response => {
      rows = rows.concat(response.data);
      let r = null;
      if (rows.length == 0) {
        return new Promise((resolve, reject) => {
          reject(`No product found matching ${JSON.stringify(product)}`);
        });
      }
      else if (rows.length == 1) {
        r = rows[0];
      } else {
        rows.forEach(row => {
          if (!r && product.sku && product.sku == row.sku) {
            r = row;
          } else if (!r && product.upc && product.upc == row.upc) {
            r = row;
          }
        });
      }
      if (r) {
        Object.keys(product).filter(f => { return f != 'sku' && f != 'upc' }).forEach(k => {
          r[k] = product[k];
        });

        return new Promise((resolve, reject) => {
          return requestPromise(updateURL(r)).then(body => {
            resolve(body.data);
          });
        });
      }

      const promises = [];
      if (response.meta && response.meta.pagination && response.meta.pagination.current_page < response.meta.pagination.total_pages) {
        for (let i = response.meta.pagination.current_page + 1; i <= response.meta.pagination.total_pages; i++) {

          promises.push(
            requestPromise(retrieveURL(product.upc ? product.upc : product.sku, i))
          );
        }
        return Promise.all(promises)
      }
      return new Promise((resolve, reject) => {
        resolve([response]);
      });
    })
    .then(responses => {

      let r = null;
      return new Promise((resolve, reject) => {

        for (let i = 0; i < responses.length; i++) {
          const response = responses[i];
          rows = rows.concat(response.data);
        }

        rows.forEach(row => {
          if (!r && product.sku && product.sku == row.sku) {
            r = row;
          } else if (!r && product.upc && product.upc == row.upc) {
            r = row;
          }
        });

        if (! r) {
          reject(`No products found matching ${JSON.stringify(product)}`);
          return;
        }

        Object.keys(product).filter(f => { return f != 'sku' && f != 'upc' }).forEach(k => {
          r[k] = product[k];
        });

        return requestPromise(updateURL(r)).then(body => {
          resolve(body.data);
        });
      });
    });
  }

  return requestPromise(createURL(product))
  .then(body => {

    product.id = body.data.id;

    const promises = [];
    if (product.meta == null || product.meta.length == 0) {
      return new Promise((resolve, reject) => {
        resolve(product);
      });
    }

    product.meta.forEach(m => {
      if (! m.permission_set) { m.permission_set = 'write_and_sf_access'; }
      if (! m.namespace) { m.namespace = 'global'; }
      promises.push(
        requestPromise(productMetaURL(product, m))
      );
    });

    return Promise.all(promises);
  }).then(response => {
    return requestPromise(optionsURL(product));
  })
  .then(response => {

    const promises = [];
    // meta fields
    for (let i = 0; i < response.data.length; i++) {
      const o = response.data[i];
      const v = product.variants.filter(x => { return x.sku == o.sku; });
      if (v.length == 0 || v[0].meta == null || v[0].meta.length == 0) { continue; }
      v[0].meta.forEach(m => {
        if (! m.permission_set) { m.permission_set = 'write_and_sf_access'; }
        if (! m.namespace) { m.namespace = 'global'; }
        promises.push(
          requestPromise(variantMetaURL(product, { id: o.id }, m))
        );
      });
    }

    // loop through all options in the first variant and make them the default
    const d = response.data[0];
    for (let i = 0; i < d.option_values.length; i++) {
      const obj = defaultOptionURL(product, { id : d.option_values[i].option_id }, d.option_values[i].id);
      promises.push(
        requestPromise(obj)
      );
    }


    return Promise.all(promises);
  })
  .then(response => {

    if (product.custom && product.custom.length) {
      let obj = customFieldsURL(product);
      return requestPromise(obj);
    }

    return new Promise((resolve, reject) => { resolve(null); });
  })
  .then(body => {

    // photos

    if (body) {
      product.custom_fields = body.data;
      delete product.custom;
    }

    const promises = [];
    if (! product.photos) {
      return new Promise((resolve, reject) => { resolve([]); });
    }
    for (let i = 0; i < product.photos.length; i++) {
      product.photos[i].product_id = product.id;
      product.photos[i].sort_order = i;

      const obj = imageURL(product, i);

      promises.push(
        requestPromise(obj)
      );
    }
    return Promise.all(promises);
  })
  .then(result => {

    delete product.photos;
    product.images = result.map(r => { return r.data; });
    return new Promise((resolve, reject) => {
      resolve(product);
    });
  })
  .catch(e => {
    return new Promise((resolve, reject) => {
      reject(e.response ? e.response.body : e.statusCode);
    })
  });
};

module.exports = product;

if (require.main === module) {
  if (process.argv.length < 3) {
    console.error(`🚫 no query provided`);
    return;
  }

}
