const
  config = require('./config');
  requestPromise = require('request-promise'),
  request = require('request');


const search = (query, options) => {

  if (! options || ! options.env) {
    return new Promise((resolve, reject) => {
      reject('No env provided');
    });
  }

  const env = config.env[options.env];
  if (! env) {
    return new Promise((resolve, reject) => {
      reject(`Invalid env ${key}`);
    });
  }

  const key = options.env;

  const obj = {
    url: `${config.api.domain}/${env.id}/v3/catalog/products/?include=variants&keyword=${query}${options.brand != null ? `&brand_id=${options.brand}` : ''}`,
    json: true,
    method: 'get',
    headers: config.api.headers(key)
  };
//  console.log(obj);
  return obj;
};

if (require.main === module) {
  if (process.argv.length < 3) {
    console.error(`🚫 no query provided`);
    return;
  }

  const ENV = 'prod';
  const q = process.argv[2];
  const brand = process.argv.length > 3 ? process.argv[3] : null;

  requestPromise(search(q, { env: ENV, brand: brand }))
  .then((response) => {

    const rows = response.data;
    rows.forEach(r => {
      console.log(JSON.stringify(r, null, ' '));
//      console.log(`sku ${r.sku} : ${r.is_visible ? "👁" : "👁‍🗨"}  ${r.name} 📂 ${r.categories.join(' ')}`);
    });
  })
  .catch(e => {
    console.error(`🚫 ${e}`);
  });
}

module.exports = search;
