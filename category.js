const
  config = require('./config'),
  requestPromise = require('request-promise'),
  request = require('request');

const key = 'prod';
const env = config.env[key];

const getCategory = (id) => {

  return {
    url: `${config.api.domain}/${env.id}/v3/catalog/categories/${id}`,
    json: true,
    method: 'get',
    headers: config.api.headers(key)
  };
};

if (process.argv.length < 3) {
  console.log(JSON.stringify(config.categories, null, '  '));
  return;
};

const category = parseInt(process.argv[2], 10);
const brand = process.argv.length > 3 ? config.brands[process.argv[3]] : null;

/**
 *
 */
const getProducts = function(category, brand, page){

  if (! page) {
    page = 1;
  }

  return new Promise(
    function(resolve, reject) {

      let url = `${config.api.domain}/${env.id}/v3/catalog/products?categories:in=${category}&page=${page}`;
      if (brand) {
        url += `&brand_id=${brand}`;
      }

      request({
        headers: config.api.headers(key),
        json: true,
        uri: url,
        method: 'GET',
      },
      function(err, res, body) {
        if (err || res.statusCode != 200){
          reject(err || { statusCode: res.statusCode });
          return;
        }
        resolve(body);
      });
    }
  )
};

async function getAllProducts(category, brand) {

  let page = 1;
  let res = await getProducts(category, brand, page);
  let all = [];
  res.data.forEach(b => {
    all.push(b);
  });

  while (res.meta.pagination.current_page < res.meta.pagination.total_pages) {
    page++;
    res = await getProducts(category, brand, page);
    res.data.forEach(b => {
      all.push(b);
    });
  }
  return all;
}

requestPromise(getCategory(category))
.then((response) => {

  console.log(`${response.data.name} : https://${env.domain}${response.data.custom_url.url}`);

 const products = getAllProducts(category, brand);

 products.then((data) => {
   console.log(`${response.data.name} (${data.length}) : https://${env.domain}${response.data.custom_url.url}`);
   console.log(data.map(b => `  ${b.sku} : ${b.is_visible ? "👁" : "👁‍🗨"}  ${b.name} 📂 ${b.categories.join(' ')}`).join("\n"));
 });
});

return;
