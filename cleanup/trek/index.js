const supplier = 2130;

const arr = [

  {
    sku: "745889395679",
    categories: [2425],
  },

  {
    sku: "012527024216", // small part
    categories: [ 1917 ]
  },
  {
    sku: "012527001095", // saris fork mount
    categories: [ 1917 ]
  },
  {
    sku: 766759000424, // ring
    categories: [ 1811]
  },
  {
    sku: "768682863534", //
    categories: [ 1868]
  },
  {
    sku: 766759000417, // kmc chainring again
    categories: [ 1811]
  },
  {
    sku: 766759000196, // kmc chain aid
    categories: [ 1914]
  },
  {
    sku: 766759000073, // kmc 2-in-1
    categories: [ 1914],
  },

  {
    sku: 766759000073, // kmc 2-in-1
    categories: [ 1914],
  },
  {
    sku: 766759000042, // KMC Missing Link Chain Connector
    categories: [ 1914],
  },
  {
    sku: 766759000035, // KMC Missing Link Chain remover
    categories: [ 1914],
  },
  {
    sku: 766759000011, // KMC digital chain checker
    categories: [ 1914],
  },

  {
    sku: 745889200362,
    categories: [ 1980]
  },

  {
    sku: 601479512911,
    categories: [ 1914]
  },
  {
    sku: 601479512904,
    categories: [ 1914]
  },
  {
    sku: 601479492084,
    categories: [ 1914]
  },
  {
    sku: 601479492077,
    categories: [ 1914]
  },
  {
    sku: 601479492060,
    categories: [ 1914]
  },
  {
    sku: "766759711764",
    is_visible: false
  },
//   {
//     upc: "601842253311",
//     categories: [ 1812]
//   },
//   {
//     upc: "745889343540",
//     categories: [ 1812]
//   },
  {
    sku: "811079026590",
    categories: [ 1914]
  },
  {
    sku: "601479836871",
    categories: [ 1812]
  },
  {
    sku: "601479492022",
    categories: [ 1914]
  },
  {
    sku: "601479492022",
    categories: [ 1914]
  },
  {
    sku: "601842309636",
    categories: [ 2425]
  },
  {
    sku: "601842345023",
    categories: [ 2425]
  },
  {
    sku: "601842381540",
    categories: [ 2425]
  },
  {
    sku: "745889395679",
    categories: [ 2425]
  },
  {
    sku: "601842238288",
    categories: [ 1940]
  },
  {
    sku: "601479963478",
    categories: [ 2425]
  },

//   {
//     upc: "601479635801",
//     categories: [ 1828]
//   },


  {
    sku: "768682755907",
    categories: [ 1868]
  },
  {
    sku: "601479146772",
    categories: [ 1868]
  },
  {
    sku: "601479192991",
    categories: [ 1868]
  },
  {
    sku: "601479200733",
    categories: [ 1868]
  },
  {
    sku: "601479566341",
    categories: [ 1868]
  },
  {
    sku: "601479566358",
    categories: [ 1868]
  },
  {
    sku: "768682735374",
    categories: [ 1868]
  },
  {
    sku: "601479572847",
    categories: [ 1942]
  },
  {
    sku: "763477004567",
    categories: [ 1812],
  },
  {
    sku: "853583001199",
    categories: [ 1828],
  },
  {
    sku: "601479410392",
    categories: [ 1953],
  },
  {
    sku: "846635013967",
    categories: [ 1952],
  },
  {
    sku: "601479831784",
    categories: [ 1940],
  },
  {
    sku: "601842149232",
    categories: [ 1940],
  },
  {
    sku: "601479290437",
    categories: [ 1940],
  },
  {
    sku: "601479290437",
    categories: [ 1940],
  },
  {
    sku: "601479749317",
    categories: [ 2024],
  },
  {
    sku: "601479750696",
    categories: [ 2024],
  },
  {
    sku: "601479799190",
    categories: [ 2024],
  },
  {
    sku: "601479819560",
    categories: [ 2024],
  },
  {
    sku: "601842149225",
    categories: [ 2024],
  },
  {
    sku: "601479772148",
    categories: [ 2019],
  },
  {
    sku: "012527030354",
    categories: [ 1917],
  },
  {
    sku: "012527025091",
    categories: [ 1917],
  },
  {
    sku: "896581002577",
    categories: [ 1917],
  },
  {
    sku: "601842826188",
    categories: [ 2011, 2036]
  },
  {
    sku: "601842113325",
    categories: [ 2011, 2036]
  },
  {
    sku: "601479683758",
    categories: [ 2014]
  },
  {
    sku: "799403203100",
    categories: [ 1999, 1917]
  },
  {
    sku: "601842371459",
    categories: [ 1914]
  },
  {
    sku: "601479502622",
    categories: [ 1914]
  },
  {
    sku: "601479625093",
    categories: [ 1914]
  },
//   {
//     sku: "888818603671",
//     categories: [ 1940]
//   },
//   {
//     sku: "888818495221",
//     categories: [ 1940]
//   },
  {
    sku: "191972607141",
    categories: [ 1923 ]
  },
  {
    sku: "191972607134",
    categories: [ 1923]
  },
  {
    sku: "012527001095",
    categories: [1917]
  },
  {
    sku: "012527024841",
    categories: [ 2428]
  },
  {
    sku: "817966010758",
    categories: [ 2428 ]
  },
  {
    sku: "817966011038",
    categories: [ 2428 ]
  },
  {
    sku: "012527018734",
    categories: [ 2430 ]
  },
  {
    sku: "886798034904",
    categories: [ 1971 ]
  },
  {
    sku: "886798035000",
    categories: [ 1971 ]
  },
  {
    sku: "845136081666",
    categories: [ 1971 ]
  },
  {
    sku: "845136078857",
    categories: [ 1971 ]
  },
  {
    sku: "845136078727",
    categories: [ 1971 ]
  },
  {
    sku: "845136068216",
    categories: [ 1971 ]
  },
  {
    sku: "601842298282",
    categories: [ 1918 ]
  },
  {
    sku: "601842407141",
    categories: [ 1918 ]
  },
  {
    sku: "601479708116",
    categories: [ 1918 ]
  },
  {
    sku: "601479708109",
    categories: [ 1918 ]
  },
  {
    sku: "886798029351",
    categories: [ 1918 ]
  },
  {
    sku: "601842684535",
    categories: [ 1971 ]
  },
  {
    sku: "845136044203",
    categories: [ 1971 ]
  },
  {
    sku: "845136044227",
    categories: [ 1971 ]
  },
  {
    sku: "845136044180",
    categories: [ 1971 ]
  },
  {
    sku: "845136044241",
    categories: [ 1971 ]
  },
  {
    sku: "845136044326",
    categories: [ 1971 ]
  },
  {
    sku: "721226",
    categories: [ 1918 ]
  },
  {
    sku: "721154",
    categories: [ 1918 ]
  },
  {
    sku: "720584",
    categories: [ 1918 ]
  },
  {
    sku: "720583",
    categories: [ 1918 ]
  },
  {
    sku: "601842277232",
    categories: [ 1918 ]
  },
  {
    sku: "601842498088",
    categories: [ 1918 ]
  },
  {
    sku: "601842498095",
    categories: [ 1918 ]
  },
  {
    sku: "601842685822",
    categories: [ 1918 ]
  },
  {
    sku: "4250450721284",
    categories: [ 1918 ]
  },
  {
    sku: "4250450723707",
    categories: [ 1918 ]
  },
  {
    sku: "4250450722267",
    categories: [ 1918 ]
  },
//   {
//     sku: "857373008184",
//     categories: [ 1917 ]
//   },
//   {
//     sku: "857373008177",
//     categories: [ 1917 ]
//   },
  {
    sku: "601842498088",
    categories: [ 1918 ]
  },
  {
    sku: "601842498088",
    categories: [ 1918 ]
  },
  {
    sku: "601842498095",
    categories: [ 1918 ]
  },
  {
    sku: "601842684535",
    categories: [ 1971 ]
  },
  {
    sku: "601842252383",
    categories: [ 1868 ]
  },
  {
    sku: "601479890361",
    categories: [ 1918 ]
  },
  {
    sku: "845136081666",
    categories: [ 1971 ],
  },
  {
    sku: "601479462841",
    categories: [ 1918 ],
  },
  {
    sku: "601842298282",
    categories: [ 1918 ],
  },
  {
    sku: "601842407141",
    categories: [ 1918 ],
  },
  {
    sku: "601842839256",
    categories: [ 1868 ],
  },
  {
    sku: "845136044241",
    categories: [ 1971 ],
  },
  {
    sku: "845136078727",
    categories: [ 1971 ],
  },
  {
    sku: "845136078857",
    categories: [ 1971 ],
  },
//   {
//     sku: "00601479808687",
//     categories: [ 1942 ],
//   },
  {
    sku: "094664035683",
    categories: [ 1940 ],
  },
//   {
//     sku: "00745889657715",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00745889657470",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479928392",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479686414",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479454976",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479418190",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479418183",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479388127",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479217151",
//     categories: [ 1926 ],
//   },
//   {
//     sku: "00601479114351",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479444892",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479554867",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699041271",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699041301",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699050952",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699050969",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699050983",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00702699050990",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479338191",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479213443",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00871828000042",
//     categories: [ 1940 ],
//   },
//   {
//     sku: "00601479089697",
//     categories: [ 1952 ],
//   },
  {
    sku: "799403308102",
    categories: [ 1917 ],
  },

  {
    sku: "601479770083",
    categories: [ 1841 ],
  },
  {
    sku: "601842429990",
    categories: [ 1841 ],
  },
  {
    sku: "601479671304",
    categories: [ 1841 ],
  },
  {
    sku: "698545293683",
    categories: [ 1999, 2008 ],
  },
  {
    sku: "811079024558",
    categories: [ 1914 ],
  },
  {
    sku: "710845794780",
    categories: [ 1914 ],
  },
  {
    sku: "888818495221",
    categories: [ 1940 ],
  },
  {
    sku: "888818603671",
    categories: [ 1940 ],
  },
  {
    sku: "768682769249",
    categories: [ 1812 ],
  },
  {
    sku: "35713",
    categories: [ 1841 ],
  },
  {
    sku: "35712",
    categories: [ 1841 ],
  },
  {
    sku: "35711",
    categories: [ 1841 ],
  },
  {
    sku: "35710",
    categories: [ 1841 ],
  },
  {
    sku: "35705",
    categories: [ 1841 ],
  },
  {
    sku: "29402",
    categories: [ 1841 ],
  },
  {
    sku: "25480",
    categories: [ 1841 ],
  },
  {
    sku: "12641",
    categories: [ 1841 ],
  },
//
  {
    sku: "29403",
    categories: [ 1841 ],
  },
  {
    sku: "22378",
    categories: [ 1841 ],
  },
  {
    sku: "29402",
    categories: [ 1841 ],
  },
  {
    sku: "25480",
    categories: [ 1841 ],
  },
  {
    sku: "710845802874",
    categories: [ 1868 ],
  },
  {
    sku: "14289",
    categories: [ 1914 ],
  },
  {
    sku: "810006805277",
    categories: [ 1914 ],
  },
  {
    sku: "812719026789",
    categories: [ 1914 ],
  },
  {
    sku: "810006801798",
    categories: [ 1914 ],
  },
  {
    sku: "720018002291",
    categories: [ 1956 ],
  },
  {
    sku: "720018003267",
    categories: [ 1956 ],
  },
  {
    sku: "720018003274",
    categories: [ 1956 ],
  },
  {
    sku: "720018003267",
    categories: [ 1956 ],
  },
  {
    sku: "720018002277",
    categories: [ 1956 ],
  },
  {
    sku: "601842762486",
    categories: [ 1956 ],
  },
  {
    sku: "720018002260",
    categories: [ 1956 ],
  },
  {
    sku: "610990200009",
    categories: [ 1914 ],
  },
//   {
//     sku: "00836572004630",
//     categories: [ 1868 ],
//   },
//   {
//     sku: "00836572007181",
//     categories: [ 1868 ],
//   },
  {
    sku: "31231",
    categories: [ 2218 ],
  },
  {
    sku: "820103329118",
    categories: [ 2218 ],
  },
  {
    sku: "820103329125",
    categories: [ 2218 ],
  },
  {
    sku: "820103329163",
    categories: [ 2218 ],
  },
  {
    sku: "820103329170",
    categories: [ 2218 ],
  },
  {
    sku: "854505008142",
    categories: [ 2218 ],
  },
  {
    sku: "858690007836",
    categories: [ 2218 ],
  },
  {
    sku: "858690007898",
    categories: [ 2218 ],
  },
  {
    sku: "32152",
    categories: [ 2218 ],
  },
  {
    sku: "11113",
    categories: [ 2218 ],
  },
  {
    sku: "11114",
    categories: [ 2219 ],
  },
  {
    sku: "11122",
    categories: [ 2218 ],
  },
  {
    sku: "13557",
    categories: [ 2218 ],
  },
  {
    sku: "14215",
    categories: [ 2218 ],
  },
  {
    sku: "14849",
    categories: [ 2218 ],
  },
  {
    sku: "23138",
    categories: [ 2218 ],
  },
  {
    sku: "22435",
    categories: [ 2218 ],
  },
  {
    sku: "23139",
    categories: [ 2218 ],
  },
  {
    sku: "23140",
    categories: [ 2218 ],
  },
  {
    sku: "23143",
    categories: [ 2218 ],
  },
  {
    sku: "25076",
    categories: [ 2218 ],
  },
  {
    sku: "25083",
    categories: [ 2219 ],
  },
  {
    sku: "25084",
    categories: [ 2218 ],
  },
  {
    sku: "28280",
    categories: [ 2218 ],
  },
  {
    sku: "31231",
    categories: [ 2218 ],
  },
  {
    sku: "31233",
    categories: [ 2218 ],
  },
  {
    sku: "32146",
    categories: [ 2218 ],
  },
  {
    sku: "34637",
    categories: [ 2218 ],
  },
  {
    sku: "34638",
    categories: [ 2218 ],
  },
  {
    sku: "36786",
    categories: [ 2218 ],
  },
  {
    sku: "36788",
    categories: [ 2218 ],
  },
  {
    sku: "14585",
    categories: [ 2218 ],
  },
  {
    sku: "14586",
    categories: [ 2218 ],
  },
  {
    sku: "811660022970",
    categories: [ 2218 ],
  },
  {
    sku: "811660022956",
    categories: [ 2218 ],
  },
  {
    sku: "811660022949",
    categories: [ 2218 ],
  },
  {
    sku: "810815024326",
    categories: [ 2218 ],
  },
  {
    sku: "810815024340",
    categories: [ 2218 ],
  },
  {
    sku: "810815024326",
    categories: [ 2218 ],
  },
  {
    sku: "810815024357",
    categories: [ 2218 ],
  },
  {
    sku: "810815020502",
    categories: [ 2219 ],
  },
  {
    sku: "810815020168",
    categories: [ 2219 ],
  },
  {
    sku: "769493101082",
    categories: [ 2218 ],
  },
  {
    sku: "769493101488",
    categories: [ 2218 ],
  },
  {
    sku: "769493101488",
    categories: [ 2218 ],
  },
  {
    sku: "811660022932",
    categories: [ 2218 ],
  },
  {
    sku: "811660022468",
    categories: [ 2218 ],
  },
  {
    sku: "811660022383",
    categories: [ 2218 ],
  },
  {
    sku: "811660021379",
    categories: [ 2218 ],
  },
  {
    sku: "811660020860",
    categories: [ 2218 ],
  },
  {
    sku: "712038927920",
    categories: [ 2218 ],
  },
  {
    sku: "712038927937",
    categories: [ 2218 ],
  },
  {
    sku: "718122537648",
    categories: [ 2218 ],
  },
  {
    sku: "712038343492",
    categories: [ 2218 ],
  },
  {
    sku: "045635168602",
    categories: [ 2218 ],
  },
  {
    sku: "045635168626",
    categories: [ 2218 ],
  },
  {
    sku: "045635168633",
    categories: [ 2218 ],
  },
  {
    sku: "045635168664",
    categories: [ 2218 ],
  },
  {
    sku: "045635168756",
    categories: [ 2218 ],
  },
  {
    sku: "689076279031",
    categories: [ 2218 ],
  },
  {
    sku: "689076343619",
    categories: [ 2218 ],
  },
  {
    sku: "689076343718",
    categories: [ 2218 ],
  },
  {
    sku: "14203",
    categories: [ 1999, 2008 ],
  },
  {
    sku: "13349",
    categories: [ 1999, 2008 ],
  },
  {
    sku: "012527018888",
    categories: [ 2425 ],
  },
  {
    sku: "31577",
    categories: [ 2013 ],
  },
  {
    sku: "34103",
    categories: [ 1971 ],
  },
  {
    sku: "34097",
    categories: [ 1971 ],
  },
  {
    sku: "35558",
    categories: [ 1918 ],
  },
  {
    sku: "35557",
    categories: [ 1918 ],
  },
  {
    sku: "35556",
    categories: [ 1918 ],
  },
  {
    sku: "35549",
    categories: [ 1918 ],
  },
  {
    sku: "14798",
    categories: [ 1918 ],
  },
  {
    sku: "29609",
    categories: [ 1971 ],
  },
  {
    sku: "29619",
    categories: [ 1971 ],
  },
  {
    sku: "29620",
    categories: [ 1971 ],
  },
  {
    sku: "29621",
    categories: [ 1971 ],
  },
  {
    sku: "29622",
    categories: [ 1971 ],
  },
  {
    sku: "29623",
    categories: [ 1971 ],
  },
  {
    sku: "29624",
    categories: [ 1971 ],
  },
  {
    sku: "29627",
    categories: [ 1971 ],
  },
  {
    sku: "31737",
    categories: [ 1971 ],
  },
  {
    sku: "34041",
    categories: [ 1971 ],
  },
  {
    sku: "34042",
    categories: [ 1971 ],
  },
  {
    sku: "34043",
    categories: [ 1971 ],
  },
  {
    sku: "37364",
    categories: [ 2027 ],
  },
  {
    sku: "34659",
    categories: [ 1996 ],
  },
  {
    sku: "34655",
    categories: [ 2005 ],
  },
  {
    sku: "34654",
    categories: [ 2004 ],
  },
  {
    sku: "34653",
    categories: [ 1995 ],
  },
  {
    sku: "34508",
    categories: [ 1999 ],
  },
  {
    sku: "34513",
    categories: [ 2008 ],
  },
  {
    sku: "32748",
    categories: [ 2028 ],
  },
  {
    sku: "34654",
    categories: [ 2004 ],
  },
  {
    sku: "34659",
    categories: [ 1996 ],
  },
  {
    sku: "34655",
    categories: [ 2005 ],
  },
  {
    sku: "34653",
    categories: [ 1995 ],
  },
  {
    sku: "34654",
    categories: [ 2004 ],
  },
  {
    sku: "33678",
    categories: [ 2014 ],
  },
  {
    sku: "34513",
    categories: [ 2008 ],
  },
  {
    sku: "32746",
    categories: [ 1996 ],
  },
  {
    sku: "32747",
    categories: [ 2005 ],
  },
  {
    sku: "27179",
    categories: [ 2005 ],
  },
  {
    sku: "27178",
    categories: [ 2004 ],
  },
  {
    sku: "14970",
    categories: [ 1999 ],
  },
  {
    sku: "24883",
    categories: [ 2013 ],
  },
  {
    sku: "34507",
    categories: [ 1999 ],
  },
  {
    sku: "14964",
    categories: [ 1995 ],
  },
  {
    sku: "14964",
    categories: [ 1995 ],
  },
  {
    sku: "23170",
    categories: [ 1940 ],
  },
  {
    sku: "33372",
    categories: [ 2047 ],
  },
  {
    sku: "33276",
    categories: [ 2047 ],
  },
  {
    sku: "22874",
    categories: [ 2047 ],
  },
  {
    sku: "31235",
    categories: [ 2047 ],
  },
  {
    sku: "601479759668",
    categories: [ 1820 ],
  },
  {
    sku: "012527018932",
    categories: [ 2425 ],
  },
  {
    sku: "601842375907",
    categories: [ 1911 ],
  },
  {
    sku: "31390",
    categories: [ 1812 ],
  },
  {
    sku: "24307",
    categories: [ 1812 ],
  },
  {
    sku: "32011",
    categories: [ 1918 ],
  },
  {
    sku: "32010",
    categories: [ 1918 ],
  },
  {
    sku: "812719029841",
    categories: [ 1952 ],
  },
  {
    sku: "24977",
    categories: [ 1820 ],
  },
  {
    sku: "12787",
    categories: [ 1820 ],
  },
  {
    sku: "710845783562",
    categories: [ 1820 ],
  },
  {
    sku: "601842451809",
    categories: [ 1820 ],
  },
  {
    sku: "601842145845",
    categories: [ 1820 ],
  },
  {
    sku: "33734",
    categories: [ 2004 ],
  },
  {
    sku: "27192",
    categories: [ 2008 ],
  },
  {
    sku: "13032",
    categories: [ 1996 ],
  },
  {
    sku: "33411",
    categories: [ 2024 ],
  },
  {
    sku: "28847",
    categories: [ 2024 ],
  },
  {
    sku: "28846",
    categories: [ 2024 ],
  },
  {
    sku: "14885",
    categories: [ 2024 ],
  },
  {
    sku: "14886",
    categories: [ 2024 ],
  },
  {
    sku: "14368",
    categories: [ 2024 ],
  },
  {
    sku: "14157",
    categories: [ 2024 ],
  },
  {
    sku: "811079027276",
    categories: [ 1914 ],
  },
  {
    sku: "601479671489",
    categories: [ 1841 ],
  },
  {
    sku: "840031005243",
    categories: [ 1868 ],
  },
  {
    sku: "601842505861",
    categories: [ 1868 ],
  },
  {
    sku: "35867",
    categories: [ 1777 ],
  },
  {
    sku: "35866",
    categories: [ 1777 ],
  },
  {
    sku: "33241",
    categories: [ 1777 ],
  },
  {
    sku: "33342",
    categories: [ 1782 ],
  },
  {
    sku: "1914",
    categories: [ 1782 ],
  },
  {
    sku: "601479129096",
    categories: [ 2425 ],
  },
  {
    sku: "601479868117",
    categories: [ 2424 ],
  },
  {
    sku: "601479868100",
    categories: [ 2424 ],
  },
  {
    sku: "601479868087",
    categories: [ 2424 ],
  },
  {
    sku: "601479868070",
    categories: [ 2424 ],
  },
  {
    sku: "601479802043",
    categories: [ 2024 ],
  },
  {
    sku: "601479698714",
    categories: [ 1828 ],
  },
  {
    sku: "37295",
    categories: [ 1828 ],
  },
  {
    sku: "24737",
    categories: [ 2424 ],
  },
  {
    sku: "601479868049",
    categories: [ 2424 ],
  },
  {
    sku: "601479867844",
    categories: [ 2424 ],
  },
  {
    sku: "601479867837",
    categories: [ 2424 ],
  },
  {
    sku: "601479867813",
    categories: [ 2424 ],
  },
  {
    sku: "601479011193",
    categories: [ 2424 ],
  },
  {
    sku: "14832",
    categories: [ 1828 ],
  },
  {
    sku: "836572007181",
    categories: [ 1868 ],
  },
  {
    sku: "601479015122",
    categories: [ 1841 ],
  },
  {
    sku: "872299046539",
    categories: [ 1922 ],
  },
  {
    sku: "872299043064",
    categories: [ 1922 ],
  },
  {
    sku: "872299043026",
    categories: [ 1922 ],
  },
  {
    sku: "872299043019",
    categories: [ 1922 ],
  },
  {
    sku: "840840009173",
    categories: [ 1922 ],
  },
  {
    sku: "840840007940",
    categories: [ 1922 ],
  },
  {
    sku: "091021771130",
    categories: [ 1922 ],
  },
  {
    sku: "601479217663",
    categories: [ 1868 ],
  },
  {
    sku: "24670",
    categories: [ 1940 ],
  },
  {
    sku: "859402006628",
    categories: [ 1940 ],
  },
  {
    sku: "601842821299",
    categories: [ 1940 ],
  },
  {
    sku: "601842674987",
    categories: [ 1940 ],
  },
  {
    sku: "601842674970",
    categories: [ 1940 ],
  },
  {
    sku: "601842530320",
    categories: [ 1940 ],
  },
  {
    sku: "601842154762",
    categories: [ 1940 ],
  },
  {
    sku: "601479932771",
    categories: [ 1940 ],
  },
  {
    sku: "601479392148",
    categories: [ 1940 ],
  },
  {
    sku: "601479388127",
    categories: [ 1940 ],
  },
  {
    sku: "601479225798",
    categories: [ 1940 ],
  },
  {
    sku: "601479200399",
    categories: [ 1940 ],
  },
  {
    sku: "012527018956",
    categories: [ 2430 ],
  },
  {
    sku: "601842530313",
    categories: [ 1940 ],
  },
  {
    sku: "012527018741",
    categories: [ 2430 ],
  },
  {
    sku: "012527018741",
    categories: [ 2430 ],
  },
  {
    sku: "012527018796",
    categories: [ 2430 ],
  },
//   {
//     sku: "31577",
//     categories: [ 2430 ],
//   },
  {
    sku: "850014080273",
    categories: [ 2434 ],
  },
  {
    sku: "850014080020",
    categories: [ 2434 ],
  },
  {
    sku: "40393",
    categories: [ 1995 ],
  },
  {
    sku: "27921",
    categories: [ 1995 ],
  },
  {
    sku: "01715",
    categories: [ 1914 ],
  },
  {
    sku: "22822",
    categories: [ 2036 ],
  },
  {
    sku: "012527018833",
    categories: [ 2428 ],
  },
  {
    sku: "8714895045351",
    categories: [ 2428 ],
  },
  {
    sku: "8714895045344",
    categories: [ 2428 ],
  },
  {
    sku: "36093",
    categories: [ 1972 ],
  },
  {
    sku: "601842585764",
    is_visible: false
  },
  {
    sku: "32233",
    categories: [ 1868 ],
  },




];

module.exports = {
  supplier: supplier,
  rows: arr.map(obj => {
    return obj;
    if (obj.categories) { obj.categories.push(supplier); } return obj;
  })
};


