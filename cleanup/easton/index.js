const supplier = 2167;

const arr = [
  {
    sku: "768686876929",
    categories: [1918],
  },
  {
    sku: "fox-free-the-trail-t-shirt",
    categories: [1998],
  },
  {
    sku: "digicam-tee",
    categories: [1998],
  },
  {
    sku: "821973381749",
    categories: [1827],
  },
  {
    sku: "611056142653",
    categories: [1836],
  },
  {
    sku: "611056143841",
    categories: [1836],
  },
  {
    sku: "611056143919",
    categories: [1836],
  },
  {
    sku: "611056147641",
    categories: [1836],
  },
  {
    sku: "611056148006",
    categories: [1836],
  },
  {
    sku: "611056191477",
    categories: [1914],
  },
  {
    sku: "611056191460",
    categories: [1914],
  },
  {
    sku: "611056146521",
    categories: [1836],
  },
  {
    sku: "611056146354",
    categories: [1836],
  },
  {
    sku: "611056146323",
    categories: [1836],
  },
  {
    sku: "611056146316",
    categories: [1836],
  },
  {
    sku: "cassette-body-grease",
    categories: [1912],
  },
  {
    sku: "textured_sleeveless_womens_tee",
    categories: [ 2004 ],
  },
  {
    sku: "womens-flannel-black-orange",
    categories: [ 2004 ],
  },
  {
    sku: "triumph_womens_tank",
    categories: [ 2004 ],
  },
  {
    sku: "textured_womens_ss_boxy_tee",
    categories: [ 2004 ],
  },
  {
    sku: "tailed_youth_ss_tee",
    categories: [ 2027 ],
  },
  {
    sku: "stacked_ss_tee",
    categories: [ 1998 ],
  },
  {
    sku: "logo_youth_pullover_hoody",
    categories: [ 2436 ],
  },
  {
    sku: "fox-womens-ride-3-0-t-shirt",
    categories: [ 2007 ],
  },
  {
    sku: "icon_youth_ss_tee",
    categories: [ 2436 ],
  },
  {
    sku: "all_day_womens_pullover_hoody",
    categories: [ 2007 ],
  },
  {
    sku: "alpine_softshell_jacket",
    categories: [ 2000 ],
  },
  {
    sku: "fox-all-day-crew-neck-french-terry-sweatshirt",
    categories: [ 1998 ],
  },
  {
    sku: "fox-bike-shop-mechanic-shirt",
    categories: [ 1998 ],
  },
  {
    sku: "fox-digicam-womens-tee",
    categories: [ 2007 ],
  },
  {
    sku: "fox-terrys-hoodie-heather-grey-men",
    categories: [ 1998 ],
  },
  {
    sku: "fox-terrys-hoodie-heather-grey-women",
    categories: [ 2007 ],
  },
  {
    sku: "pullover_hoody",
    categories: [ 1998 ],
  },
  {
    sku: "everyday-ls-flannel-shirt",
    categories: [ 1998 ],
  },






];

const seatpostParts = [
  '611056180259',
  '611056180242',
  '611056180396',
  '611056180235',
  '611056180228',
  '611056180211',
  '611056180198',
  '611056180181',
  '611056180075',
  '611056180068',
  '611056180051',
  '611056180044',
  '611056179895',
  '611056170588',
  '821973401171',
  '821973401164',
  '821973401157',
  '821973401140',
  '821973401058',
  '821973401041',
  '821973401034',
  '821973401027',
  '821973401010',
  '821973401003',
  '821973400990',
  '821973400983',
  '611056180426',
  '611056180419',
  '611056180402',
];

seatpostParts.forEach(c => {
  arr.push({ sku: c, categories: [1867] });
});



module.exports = {
  supplier: supplier,
  rows: arr.map(obj => {
    return obj;
    if (obj.categories) { obj.categories.push(supplier); } return obj;
  })
};

