const supplier = 2131;

const arr = [

//   {
//     sku: "091021343023", // low rider pro
//     categories: [ 1917 ]
//   },
//   {
//     sku: "091021529977", // Locking Bed-Rider Add-On Block
//     categories: [ 1917 ]
//   },
//   {
//     sku: "091021784154", //  Thule Locking Low Rider
//     categories: [ 1917 ]
//   },
//   {
//     sku: "091021821170", //  Thule Locking Low Rider
//     categories: [ 1917 ]
//   },
  {
    sku: "091021099463", // Thule GateMate PRO truck bed bike rack black
    categories: [ 1923  ]
  },
  {
    sku: "085854250351", // sapling rain cover
    categories: [ 1922 ]
  },
  {
    sku: "085854250320", // baby backpack agave
    categories: [ 1922 ]
  },
  {
    sku: "085854250313", // baby backpack black
    categories: [ 1922 ]
  },
//   {
//     sku: "085854240550", // rain cover
//     categories: [ 1922 ]
//   },

//   {
//     sku: "085854251464", // roundtrip mtb case
//     categories: [ 1918 ]
//   },
//   {
//     sku: "091021056695", //
//     categories: [ 1918]
//   },
  {
    sku: "091021400146", // phone mount
    categories: [ 1940]
  },
//   {
//     sku: "091021278868", // bed
//     categories: [ 1917]
//   },
  {
    sku: "091021278622",
    categories: [ 1917]
  },
//   {
//     sku: "091021305038",
//     categories: [ 1917]
//   },
  {
    sku: "091021314375",
    categories: [ 1917]
  },
  {
    sku: "00091021529977",
    categories: [ 1917 ]
  },
//   {
//     sku: "085854253260",
//     categories: [ 1918 ]
//   },
//   {
//     sku: "085854251464",
//     categories: [ 1918 ]
//   },
  {
    sku: "872299043125",
    categories: [ 1922 ]
  },
  {
    sku: "872299043347",
    categories: [ 1922 ]
  },
  {
    sku: "872299043354",
    categories: [ 1922 ]
  },
];

module.exports = {
  supplier: supplier,
  rows: arr.map(obj => {
    return obj;
    if (obj.categories) { obj.categories.push(supplier); } return obj;
  })
};

