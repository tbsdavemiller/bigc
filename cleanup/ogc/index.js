const supplier = 2037;

const arr = [
  /*   */
  {
    sku: "24525-FISHBOWL POP DISPLAY", // finishline display
    is_visible: false
  },
  {
    sku: "4055822505789",
    is_visible: false
  },
  {
    sku: "4055822505772",
    is_visible: false
  },
  {
    sku: "4055822505765",
    is_visible: false
  },
  {
    sku: "4055822022088",
    is_visible: false
  },
  {
    sku: "15610-MIXTE CHAINGUARD",
    is_visible: false,
  },
  {
    sku: "15610-MIXTE CHAINGUARD",
    is_visible: false,
  },
  {
    sku: "817905014960",
    is_visible: false,
  },
  {
    sku: "817905010917",
    is_visible: false,
  },
  {
    sku: "624316712374",
    is_visible: false,
  },
  {
    sku: "624316712367",
    is_visible: false,
  },
  {
    sku: "624316712350",
    is_visible: false,
  },
  {
    sku: "624316712343",
    is_visible: false,
  },
  {
    sku: "624316712336",
    is_visible: false,
  },
  {
    sku: "624316712329",
    is_visible: false,
  },
  {
    sku: "624316712312",
    is_visible: false,
  },
  {
    sku: "624316712305",
    is_visible: false,
  },
  {
    sku: "624316712282",
    is_visible: false,
  },
  {
    sku: "624316712275",
    is_visible: false,
  },
  {
    sku: "624316712268",
    is_visible: false,
  },
  {
    sku: "624316712251",
    is_visible: false,
  },
  {
    sku: "624316712244",
    is_visible: false,
  },
  {
    sku: "624316712237",
    is_visible: false,
  },
  {
    sku: "624316712220",
    is_visible: false,
  },
  {
    sku: "624316712206",
    is_visible: false,
  },
  {
    sku: "624316712190",
    is_visible: false,
  },
  {
    sku: "624316712183",
    is_visible: false,
  },
  {
    sku: "624316712169",
    is_visible: false,
  },
  {
    sku: "624316712152",
    is_visible: false,
  },
  {
    sku: "624316713357",
    is_visible: false,
  },
  {
    sku: "624316712954",
    is_visible: false,
  },
  {
    sku: "624316712961",
    is_visible: false,
  },
  {
    sku: "624316712978",
    is_visible: false,
  },
  {
    sku: "624316712985",
    is_visible: false,
  },
  {
    sku: "624316712992",
    is_visible: false,
  },
  {
    sku: "624316713005",
    is_visible: false,
  },
  {
    sku: "624316713012",
    is_visible: false,
  },
  {
    sku: "624316713029",
    is_visible: false,
  },
  {
    sku: "624316713036",
    is_visible: false,
  },
  {
    sku: "624316713043",
    is_visible: false,
  },
  {
    sku: "624316713050",
    is_visible: false,
  },
  {
    sku: "624316713067",
    is_visible: false,
  },
  {
    sku: "624316713074",
    is_visible: false,
  },
  {
    sku: "624316713081",
    is_visible: false,
  },
  {
    sku: "624316713098",
    is_visible: false,
  },
  {
    sku: "624316713104",
    is_visible: false,
  },
  {
    sku: "624316713111",
    is_visible: false,
  },
  {
    sku: "624316713128",
    is_visible: false,
  },
  {
    sku: "624316713135",
    is_visible: false,
  },
  {
    sku: "624316713142",
    is_visible: false,
  },
  {
    sku: "624316713159",
    is_visible: false,
  },
  {
    sku: "624316713166",
    is_visible: false,
  },
  {
    sku: "624316713173",
    is_visible: false,
  },
  {
    sku: "624316713180",
    is_visible: false,
  },
  {
    sku: "624316713197",
    is_visible: false,
  },
  {
    sku: "624316713203",
    is_visible: false,
  },
  {
    sku: "624316713234",
    is_visible: false,
  },
  {
    sku: "624316713241",
    is_visible: false,
  },
  {
    sku: "624316713265",
    is_visible: false,
  },
  {
    sku: "624316713272",
    is_visible: false,
  },
  {
    sku: "624316713289",
    is_visible: false,
  },
  {
    sku: "624316713296",
    is_visible: false,
  },
  {
    sku: "624316713302",
    is_visible: false,
  },
  {
    sku: "624316713319",
    is_visible: false,
  },
  {
    sku: "624316713326",
    is_visible: false,
  },
  {
    sku: "624316713333",
    is_visible: false,
  },
  {
    sku: "624316713340",
    is_visible: false,
  },
  {
    sku: "624316713357",
    is_visible: false,
  },
  {
    sku: "624316713371",
    is_visible: false,
  },
  {
    sku: "624316713388",
    is_visible: false,
  },
  {
    sku: "624316713395",
    is_visible: false,
  },
  {
    sku: "624316713401",
    is_visible: false,
  },
  {
    sku: "624316713418",
    is_visible: false,
  },
  {
    sku: "624316713425",
    is_visible: false,
  },
  {
    sku: "624316713432",
    is_visible: false,
  },
  {
    sku: "624316713449",
    is_visible: false,
  },
  {
    sku: "624316713456",
    is_visible: false,
  },
  {
    sku: "888818528059",
    categories: [ 1980],
  },
  {
    sku: "888818528042",
    categories: [ 1980],
  },
  {
    sku: "680031732196",
    categories: [ 1777],
  },
//   {
//     sku: "624316117865",
//     categories: [ 1777],
//   },
  {
    sku: "624316107835",
    categories: [ 1777],
  },
  {
    sku: "624316107095",
    categories: [ 1777],
  },
  {
    sku: "4055822505390",
    categories: [ 1777],
  },
  {
    sku: "4055822504874",
    categories: [ 1777],
  },
  {
    sku: "4055822504805",
    categories: [ 1777],
  },
  {
    sku: "4055822504768",
    categories: [ 1777],
  },
  {
    sku: "4055822060509",
    categories: [ 1777],
  },
  {
    sku: "036121681200",
    categories: [ 1912],
  },
  {
    sku: "4055822505833",
    categories: [ 1845],
  },
//   {
//     sku: "4055822505925",
//     categories: [ 1845],
//   },
  {
    sku: "4055822505864",
    categories: [ 1845],
  },
  {
    sku: "4055822505895",
    categories: [ 1845],
  },
//   {
//     sku: "4055822019286",
//     categories: [ 1845],
//   },
  {
    sku: "4055822019279",
    categories: [ 1845],
  },
  {
    sku: "624316128458",
    categories: [ 1868],
  },
  {
    sku: "624316128434",
    categories: [ 1868],
  },
  {
    sku: "768686151057",
    categories: [ 2009],
  },
  {
    sku: "768686151156",
    categories: [ 2009],
  },
  {
    sku: "818889022323",
    categories: [ 1940],
  },
  {
    sku: "886798909332",
    categories: [ 1973],
  },
  {
    sku: "886798031538",
    categories: [ 1973],
  },
  {
    sku: "886798025711",
    categories: [ 1973],
  },
  {
    sku: "886798025711",
    categories: [ 1973],
  },
  {
    sku: "886798025698",
    categories: [ 1973],
  },
  {
    sku: "886798020419",
    categories: [ 1973],
  },
  {
    sku: "886798019697",
    categories: [ 1973],
  },
  {
    sku: "886798019680",
    categories: [ 1973],
  },
  {
    sku: "886798013022",
    categories: [ 1973],
  },
  {
    sku: "886798013022",
    categories: [ 1973],
  },

  {
    sku: "7613052056616",
    categories: [ 1827],
  },
  {
    sku: "7613052123875",
    categories: [ 1827],
  },
  {
    sku: "7613052129365",
    categories: [ 1827],
  },
  {
    sku: "7613052179797",
    categories: [ 1827],
  },
  {
    sku: "7613052230863",
    categories: [ 1827],
  },
  {
    sku: "7613052230870",
    categories: [ 1827],
  },
  {
    sku: "7613052282879",
    categories: [ 1827],
  },
  {
    sku: "7613052291796",
    categories: [ 1827],
  },
  {
    sku: "7613052291802",
    categories: [ 1827],
  },
  {
    sku: "7613052292793",
    categories: [ 1827],
  },
  {
    sku: "7613052292823",
    categories: [ 1827],
  },
  {
    sku: "7613052320656",
    categories: [ 1827],
  },
  {
    sku: "7613052323251",
    categories: [ 1827],
  },
  {
    sku: "7613052351780",
    categories: [ 1827],
  },
  {
    sku: "7613052351797",
    categories: [ 1827],
  },
  {
    sku: "7613052361086",
    categories: [ 1827],
  },
  {
    sku: "7613052361093",
    categories: [ 1827],
  },
  {
    sku: "7613052361116",
    categories: [ 1827],
  },
  {
    sku: "7613052361123",
    categories: [ 1827],
  },
  {
    sku: "7613052361130",
    categories: [ 1827],
  },
  {
    sku: "7613052361178",
    categories: [ 1827],
  },
  {
    sku: "7613052361185",
    categories: [ 1827],
  },
  {
    sku: "7613052361208",
    categories: [ 1827],
  },
  {
    sku: "7613052361338",
    categories: [ 1827],
  },
  {
    sku: "7613052361352",
    categories: [ 1827],
  },
  {
    sku: "7613052361383",
    categories: [ 1827],
  },
  {
    sku: "7613052362014",
    categories: [ 1827],
  },
  {
    sku: "7630013900351",
    categories: [ 1827],
  },
  {
    sku: "7630013967262",
    categories: [ 1827],
  },
  {
    sku: "7630024381972",
    categories: [ 1827],
  },
  {
    sku: "7630024381989",
    categories: [ 1827],
  },
  {
    sku: "7630033858960",
    categories: [ 1827],
  },
  {
    sku: "7630033858977",
    categories: [ 1827],
  },
  {
    sku: "7630033858984",
    categories: [ 1827],
  },
  {
    sku: "5060247027616",
    categories: [ 1868],
  },
  {
    sku: "5060247026077",
    categories: [ 1836],
  },
  {
    sku: "624316095651",
    categories: [ 1828],
  },
  {
    sku: "624316095668",
    categories: [ 1828],
  },
  {
    sku: "624316029694",
    categories: [ 1828],
  },
  {
    sku: "624316730330",
    categories: [ 1973],
  },
  {
    sku: "624316033455",
    categories: [ 1836],
  },
  {
    sku: "624316033448",
    categories: [ 1836],
  },
  {
    sku: "819438019566",
    categories: [ 1841],
  },
  {
    sku: "624316737759",
    categories: [ 1868],
  },
  {
    sku: "624316117803",
    categories: [ supplier ],
    is_visible: false
  },
  {
    sku: "624316066903",
    categories: [ 1836],
  },
  {
    sku: "624316066910",
    categories: [ 1836],
  },
  {
    sku: "624316069089",
    categories: [ 1838],
  },
  {
    sku: "624316069690",
    categories: [ 1838],
  },
  {
    sku: "624316069706",
    categories: [ 1838],
  },
  {
    sku: "624316069720",
    categories: [ 1838],
  },
  {
    sku: "624316069935",
    categories: [ 1838],
  },
  {
    sku: "624316128397",
    categories: [ 1940],
  },
  {
    sku: "624316128403",
    categories: [ 1868],
  },
//   {
//     sku: "624316128427",
//     categories: [ 1973],
//   },
  {
    sku: "624316128328",
    categories: [ 1973],
  },
  {
    sku: "624316128373",
    categories: [ 1973],
  },
  {
    sku: "624316069713",
    categories: [ 1838],
  },
  {
    sku: "4055822511063",
    categories: [ 1998, 2007],
  },
  {
    sku: "768686637117",
    categories: [ 2024],
  },
  {
    sku: "361857340177",
    categories: [ 1998, 2007],
  },
  {
    sku: "4055822066464",
    categories: [ 1826],
  },
//   {
//     sku: "608597981759",
//     categories: [ 1777 ],
//   },
//   {
//     sku: "608597981742",
//     categories: [ 1777 ],
//   },
  {
    sku: "818889025164",
    categories: [ 1926 ],
  },
  {
    sku: "818889025157",
    categories: [ 1926 ],
  },
  {
    sku: "8022530002035",
    categories: [ 1912 ],
  },
  {
    sku: "641740175308",
    categories: [ 1828 ],
  },
  {
    sku: "064174021890",
    categories: [ 1832 ],
  },
  {
    sku: "4055822037907",
    categories: [ 1832 ],
  },
  {
    sku: "4055822042413",
    categories: [ 1832 ],
  },
  {
    sku: "641740027805",
    categories: [ 1828 ],
  },
  {
    sku: "036121711228",
    categories: [ 1912 ],
  },
  {
    sku: "036121711198",
    categories: [ 1912 ],
  },
  {
    sku: "4019238445077",
    categories: [ 1828 ],
  },
  {
    sku: "641740125808",
    categories: [ 1912 ],
  },
  {
    sku: "064174011080",
    categories: [ 1912 ],
  },
  {
    sku: "641740111009",
    categories: [ 1912 ],
  },
  {
    sku: "723661003561",
    categories: [ 1822 ],
  },
  {
    sku: "4250612889418",
    categories: [ 2424 ],
  },
  {
    sku: "97616",
    categories: [ 1890 ],
  },
  {
    sku: "680031722395",
    categories: [ 1828 ],
  },
  {
    sku: "5109",
    categories: [ 1828 ],
  },
  {
    sku: "83529",
    categories: [ 1828 ],
  },
//   {
//     sku: "82369",
//     categories: [ 1912 ],
//   },
  {
    sku: "81196",
    categories: [ 1833 ],
  },
  {
    sku: "70858",
    categories: [ 1833 ],
  },
  {
    sku: "70822",
    categories: [ 1832 ],
  },
  {
    sku: "70861",
    categories: [ 1828 ],
  },
  {
    sku: "81194",
    categories: [ 1832 ],
  },
  {
    sku: "19395",
    categories: [ 1832 ],
  },
  {
    sku: "19357",
    categories: [ 1833 ],
  },
  {
    sku: "19121",
    categories: [ 1832 ],
  },
  {
    sku: "2982",
    categories: [ 1828 ],
  },
  {
    sku: "107131",
    categories: [ 1912 ],
  },
  {
    sku: "107131",
    categories: [ 1912 ],
  },
  {
    sku: "104213",
    categories: [ 1912 ],
  },
//   {
//     sku: "4250612882488",
//     categories: [ 1828 ],
//   },
  {
    sku: "90444",
    categories: [ 1828 ],
  },
  {
    sku: "90440",
    categories: [ 1828 ],
  },
  {
    sku: "83569",
    categories: [ 1828 ],
  },
  {
    sku: "81180",
    categories: [ 1828 ],
  },
  {
    sku: "5059",
    categories: [ 1828 ],
  },
  {
    sku: "5880",
    categories: [ 1828 ],
  },
  {
    sku: "5883",
    categories: [ 1828 ],
  },
  {
    sku: "100920",
    categories: [ 1828 ],
  },
  {
    sku: "100874",
    categories: [ 1828 ],
  },
//   {
//     sku: "60600-S.I.R PEG",
//     categories: [ 1828 ],
//   },
  {
    sku: "4250612889449",
    categories: [ 2424 ],
  },
  {
    sku: "5125",
    categories: [ 2424 ],
  },
  {
    sku: "624316085386",
    categories: [ 1836 ],
  },
  {
    sku: "4250612883881",
    categories: [ 1836 ],
  },
  {
    sku: "4055822002394",
    categories: [ 1836 ],
  },
  {
    sku: "4055822519960",
    categories: [ 1836 ],
  },
  {
    sku: "4055822519977",
    categories: [ 1836 ],
  },
  {
    sku: "4055822519991",
    categories: [ 1836 ],
  },
//   {
//     sku: "4250612883881",
//     categories: [ 1836 ],
//   },
  {
    sku: "68303",
    categories: [ 1828 ],
  },
//   {
//     sku: "5132",
//     categories: [ 1832 ],
//   },
  {
    sku: "19099",
    categories: [ 1912 ],
  },
  {
    sku: "107858",
    categories: [ 1912 ],
  },
  {
    sku: "100304",
    categories: [ 2015 ],
  },
  {
    sku: "100294",
    categories: [ 2019 ],
  },
  {
    sku: "100314",
    categories: [ 2019 ],
  },
  {
    sku: "036121710283",
    categories: [ 1912 ],
  },
  {
    sku: "4055822017299",
    categories: [ 1868 ],
  },
  {
    sku: "4055822017305",
    categories: [ 1868 ],
  },
  {
    sku: "4250612880590",
    categories: [ 1868 ],
  },
  {
    sku: "4250612880590",
    categories: [ 1868 ],
  },
//   {
//     sku: "04710155964367",
//     categories: [ 1868 ],
//   },
//   {
//     sku: "15610-THE HAMPTON RUCKSACK",
//     categories: [ 1918 ],
//   },
  {
    sku: "886798029351",
    categories: [ 1971 ],
  },
  {
    sku: "886798035000",
    categories: [ 1971 ],
  },
  {
    sku: "886798034904",
    categories: [ 1971 ],
  },
  {
    sku: "768686603969",
    categories: [ 1828 ],
  },
  {
    sku: "768686809958",
    categories: [ 1828 ],
  },
//   {
//     sku: "74567",
//     categories: [ 1777 ],
//   },
  {
    sku: "112921",
    categories: [ 1777 ],
  },
//   {
//     sku: "100989",
//     categories: [ 1777 ],
//   },
  {
    sku: "100918",
    categories: [ 1777 ],
  },
  {
    sku: "104768",
    categories: [ 2013 ],
  },
  {
    sku: "100554",
    categories: [ 2022 ],
  },
  {
    sku: "35199",
    categories: [ 1940 ],
  },
  {
    sku: "624316741084",
    categories: [ 2425 ],
  },
  {
    sku: "624316741091",
    categories: [ 2425 ],
  },
  {
    sku: "768686055171",
    is_visible: false,
  },
//   {
//     sku: "768686713651",
//     is_visible: false,
//   },
  {
    sku: "768686836466",
    is_visible: false,
  },
  {
    sku: "768686067785",
    categories: [ 2425 ],
  },
  {
    sku: "768686207648",
    categories: [ 1942 ],
  },
  {
    sku: "768686604034",
    categories: [ 1940 ],
  },
  {
    sku: "768686604089",
    categories: [ 1940 ],
  },
  {
    sku: "113056",
    categories: [ 1868 ],
  },
  {
    sku: "624316531654",
    categories: [ 1868 ],
  },
//   {
//     sku: "624316531647",
//     categories: [ 1868 ],
//   },
//   {
//     sku: "624316531630",
//     categories: [ 1868 ],
//   },
  {
    sku: "624316079149",
    categories: [ 1868 ],
  },
  {
    sku: "000003476192",
    categories: [ 1868 ],
  },
  {
    sku: "624316083306",
    categories: [ 1868 ],
  },
  {
    sku: "624316127130",
    categories: [ 1868 ],
  },
  {
    sku: "624316128250",
    categories: [ 1868 ],
  },
  {
    sku: "624316128267",
    categories: [ 1868 ],
  },
  {
    sku: "624316128274",
    categories: [ 1868 ],
  },
  {
    sku: "000003126158",
    categories: [ 1868 ],
  },
  {
    sku: "83439",
    categories: [ 1999 ],
  },
//   {
//     sku: "81823",
//     categories: [ 1999 ],
//   },
  {
    sku: "83440",
    categories: [ 1999 ],
  },
  {
    sku: "81822",
    categories: [ 1999 ],
  },
  {
    sku: "72985",
    categories: [ 1999 ],
  },
//   {
//     sku: "40135",
//     categories: [ 1999 ],
//   },
  {
    sku: "39083",
    categories: [ 1999 ],
  },
  {
    sku: "109128",
    categories: [ 1998, 2436 ],
  },
  {
    sku: "110112",
    categories: [ 2436 ],
  },
  {
    sku: "109129",
    categories: [ 2436 ],
  },
  {
    sku: "108201",
    categories: [ 2215 ],
  },
//   {
//     sku: "109125",
//     categories: [ 2007 ],
//   },
  {
    sku: "109115",
    categories: [ 1998 ],
  },
  {
    sku: "109110",
    categories: [ 1998 ],
  },
  {
    sku: "109105",
    categories: [ 1998 ],
  },
  {
    sku: "109109",
    categories: [ 1998 ],
  },
  {
    sku: "100669",
    categories: [ 2007 ],
  },
  {
    sku: "104400",
    categories: [ 1998 ],
  },
  {
    sku: "104188",
    categories: [ 1998 ],
  },
  {
    sku: "100504",
    categories: [ 1999, 2008 ],
  },
  {
    sku: "100660",
    categories: [ 1998 ],
  },
  {
    sku: "72984",
    categories: [ 1999 ],
  },
  {
    sku: "109122",
    categories: [ 2007 ],
  },
//   {
//     sku: "109114",
//     categories: [ 1998 ],
//   },
  {
    sku: "109112",
    categories: [ 1998 ],
  },
  {
    sku: "108314",
    categories: [ 2006 ],
  },
  {
    sku: "108308",
    categories: [ 1997 ],
  },
  {
    sku: "105596",
    categories: [ 1999 ],
  },
//   {
//     sku: "105599",
//     categories: [ 1999 ],
//   },
  {
    sku: "100478",
    categories: [ 1999 ],
  },
  {
    sku: "100479",
    categories: [ 1999 ],
  },
  {
    sku: "100502",
    categories: [ 1999 ],
  },
  {
    sku: "100503",
    categories: [ 1999 ],
  },
  {
    sku: "100480",
    categories: [ 2036 ],
  },
  {
    sku: "107163",
    categories: [ 2027 ],
  },
  {
    sku: "107163",
    categories: [ 2027 ],
  },
  {
    sku: "107165",
    categories: [ 2027 ],
  },
  {
    sku: "107164",
    categories: [ 2027 ],
  },
//   {
//     sku: "100402",
//     categories: [ 2027 ],
//   },
//   {
//     sku: "100399",
//     categories: [ 2027 ],
//   },
  {
    sku: "100415",
    categories: [ 2027 ],
  },
  {
    sku: "100420",
    categories: [ 2027 ],
  },
  {
    sku: "100322",
    categories: [ 2004 ],
  },
  {
    sku: "100430",
    categories: [ 1999 ],
  },
  {
    sku: "100437",
    categories: [ 1999 ],
  },
  {
    sku: "100454",
    categories: [ 1999 ],
  },
  {
    sku: "100461",
    categories: [ 1999 ],
  },
  {
    sku: "100665",
    categories: [ 1995 ],
  },
//   {
//     sku: "100154",
//     categories: [ 2027 ],
//   },
  {
    sku: "843520162472",
    categories: [ 1998 ],
  },
  {
    sku: "768686292224",
    categories: [ 2004 ],
  },
//   {
//     sku: "768686203527",
//     categories: [ 2004 ],
//   },
//   {
//     sku: "768686203282",
//     categories: [ 2004 ],
//   },
  {
    sku: "768686203220",
    categories: [ 2004 ],
  },
//   {
//     sku: "768686203107",
//     categories: [ 2004 ],
//   },
//   {
//     sku: "768686103728",
//     categories: [ 2004 ],
//   },
//   {
//     sku: "768686103841",
//     categories: [ 2004 ],
//   },
  {
    sku: "768686103919",
    categories: [ 2004 ],
  },
  {
    sku: "768686103988",
    categories: [ 2004 ],
  },
  {
    sku: "768686104053",
    categories: [ 2004 ],
  },
  {
    sku: "768686104183",
    categories: [ 2004 ],
  },
  {
    sku: "768686104121",
    categories: [ 2004 ],
  },
  {
    sku: "680031739904",
    categories: [ 1998 ],
  },
//   {
//     sku: "624316103349",
//     categories: [ 1998 ],
//   },
//   {
//     sku: "84121",
//     categories: [ 2004 ],
//   },
//   {
//     sku: "81822",
//     categories: [ 1999 ],
//   },
//   {
//     sku: "72985",
//     categories: [ 1999 ],
//   },
//   {
//     sku: "Rivet Cs",
//     categories: [ 1999 ],
//   },
  {
    sku: "107177",
    categories: [ 2027 ],
  },
  {
    sku: "109104",
    categories: [ 1998 ],
  },

//   {
//     sku: "624316106647",
//     categories: [ 1998 ],
//   },
//   {
//     sku: "104178",
//     categories: [ 2009 ],
//   },
//   {
//     sku: "100418",
//     categories: [ 2009 ],
//   },
  {
    sku: "100392",
    categories: [ 2009 ],
  },
//   {
//     sku: "00601842632000",
//     categories: [ 2009 ],
//   },
//   {
//     sku: "75487",
//     categories: [ 1833 ],
//   },
//   {
//     sku: "104425",
//     categories: [ 1833 ],
//   },
//   {
//     sku: "768686731150",
//     is_visible: false
//   },
//   {
//     sku: "107176",
//     categories: [ 2027 ],
//   },
  {
    sku: "113056",
    categories: [ 1868 ],
  },
  {
    sku: "100286",
    categories: [ 2013 ],
  },
//   {
//     sku: "100288",
//     categories: [ 2013 ],
//   },
  {
    sku: "768686591648",
    categories: [ 2013 ],
  },
//   {
//     sku: "768686158667",
//     categories: [ 2013 ],
//   },
//   {
//     sku: "768686982347",
//     categories: [ 2013 ],
//   },
  {
    sku: "100415",
    categories: [ 2004 ],
  },
  {
    sku: "724120113524",
    categories: [ 1777 ],
  },
  {
    sku: "15610-FRONT BRAKE SET",
    categories: [ 2047 ],
  },
  {
    sku: "768114174320",
    categories: [ 1810 ],
  },
  {
    sku: "4250612879518",
    categories: [ 1911 ],
  },
  {
    sku: "680031705336",
    categories: [ 1810 ],
  },
  {
    sku: "680031727550",
    categories: [ 1810 ],
  },
  {
    sku: "624316032557",
    categories: [ 1810 ],
  },
  {
    sku: "5060514325162",
    categories: [ 1810 ],
  },
  {
    sku: "768686590078",
    categories: [ 2019 ],
  },
  {
    sku: "085854253260",
    categories: [ 1918 ],
  },
  {
    sku: "085854251464",
    categories: [ 1918 ],
  },
  {
    sku: "100913",
    categories: [ 1811 ],
  },
  {
    sku: "112945",
    categories: [ 1998 ],
  },
  {
    sku: "109123",
    categories: [ 2007 ],
  },
  {
    sku: "105599",
    categories: [ 1999 ],
  },
  {
    sku: "100453",
    categories: [ 2215 ],
  },
  {
    sku: "84082",
    categories: [ 2004 ],
  },
  {
    sku: "84128",
    categories: [ 2004 ],
  },
  {
    sku: "100414",
    categories: [ 2004 ],
  },
  {
    sku: "100413",
    categories: [ 2004 ],
  },
  {
    sku: "100452",
    categories: [ 2008 ],
  },
  {
    sku: "100388",
    categories: [ 2008 ],
  },
  {
    sku: "768686203527",
    categories: [ 2004 ],
  },
  {
    sku: "768686203282",
    categories: [ 2004 ],
  },
  {
    sku: "768686203107",
    categories: [ 2004 ],
  },
  {
    sku: "768686103841",
    categories: [ 2004 ],
  },
  {
    sku: "768686103728",
    categories: [ 2004 ],
  },
  {
    sku: "624316103349",
    categories: [ 2004 ],
  },
  {
    sku: "84141",
    categories: [ 2005 ],
  },
  {
    sku: "108313",
    categories: [ 2005 ],
  },
  {
    sku: "100181",
    categories: [ 2028 ],
  },
  {
    sku: "100175",
    categories: [ 2028 ],
  },
  {
    sku: "37955",
    categories: [ 2028 ],
  },
  {
    sku: "100395",
    categories: [ 2005 ],
  },
  {
    sku: "100400",
    categories: [ 2005 ],
  },
  {
    sku: "100419",
    categories: [ 2005 ],
  },
  {
    sku: "100390",
    categories: [ 2005 ],
  },
  {
    sku: "100387",
    categories: [ 2005 ],
  },
  {
    sku: "38093",
    categories: [ 2005 ],
  },
  {
    sku: "104177",
    categories: [ 2005 ],
  },
  {
    sku: "100393",
    categories: [ 2005 ],
  },
  {
    sku: "100410",
    categories: [ 2005 ],
  },
  {
    sku: "104176",
    categories: [ 2005 ],
  },
  {
    sku: "100382",
    categories: [ 2005 ],
  },
  {
    sku: "100380",
    categories: [ 2005 ],
  },
  {
    sku: "100328",
    categories: [ 2005 ],
  },
  {
    sku: "100321",
    categories: [ 2005 ],
  },
  {
    sku: "100319",
    categories: [ 2005 ],
  },
  {
    sku: "768686151873",
    categories: [ 2005 ],
  },
  {
    sku: "768686105371",
    categories: [ 2005 ],
  },
  {
    sku: "768686105319",
    categories: [ 2005 ],
  },
  {
    sku: "768686105258",
    categories: [ 2005 ],
  },
  {
    sku: "768686104954",
    categories: [ 2005 ],
  },
  {
    sku: "107178",
    categories: [ 2028 ],
  },
  {
    sku: "104178",
    categories: [ 2009 ],
  },
  {
    sku: "768686151309",
    categories: [ 2009 ],
  },
  {
    sku: "100398",
    categories: [ 2006 ],
  },
  {
    sku: "100394",
    categories: [ 2006 ],
  },
  {
    sku: "83604",
    categories: [ 1822 ],
  },
  {
    sku: "100868",
    categories: [ 1820 ],
  },
  {
    sku: "113054",
    categories: [ 1833 ],
  },
  {
    sku: "61945",
    categories: [ 1833 ],
  },
  {
    sku: "28020-TUBELESS VALVE",
    categories: [ 1833 ],
  },
  {
    sku: "28020-TUBELESS REFILL",
    categories: [ 1833 ],
  },
  {
    sku: "7630033892087",
    categories: [ 1822 ],
  },
  {
    sku: "7630033892070",
    categories: [ 1822 ],
  },
  {
    sku: "7630033807043",
    categories: [ 1827 ],
  },
  {
    sku: "7630033808637",
    categories: [ 1833 ],
  },
  {
    sku: "7630033807036",
    categories: [ 1827 ],
  },
  {
    sku: "7630024330703",
    categories: [ 1827 ],
  },
  {
    sku: "7624316051572",
    categories: [ 1827 ],
  },
  {
    sku: "7613052293110",
    categories: [ 1827 ],
  },
  {
    sku: "7613052293028",
    categories: [ 1827 ],
  },
  {
    sku: "7613052293011",
    categories: [ 1827 ],
  },
  {
    sku: "7613052292960",
    categories: [ 1827 ],
  },
  {
    sku: "7613052213316",
    categories: [ 1827 ],
  },
  {
    sku: "7613052199955",
    categories: [ 1827 ],
  },
  {
    sku: "7613052144412",
    categories: [ 1827 ],
  },
  {
    sku: "7613052036670",
    categories: [ 1890 ],
  },
  {
    sku: "7613052036687",
    categories: [ 1890 ],
  },
  {
    sku: "723660999995",
    categories: [ 1918 ],
  },
  {
    sku: "624316714460",
    categories: [ 1967 ],
  },
  {
    sku: "624316042167",
    categories: [ 1890 ],
  },
  {
    sku: "000001893113",
    categories: [ 1868 ],
  },
  {
    sku: "624316774181",
    categories: [ 1868 ],
  },
  {
    sku: "000000000062",
    is_visible: false
  },
  {
    sku: "624316116530",
    categories: [ 1868 ],
  },
  {
    sku: "624316116523",
    categories: [ 1868 ],
  },
  {
    sku: "3179",
    categories: [ 1841 ],
  },
  {
    sku: "3182",
    categories: [ 1841 ],
  },
  {
    sku: "610990330065",
    categories: [ 1914 ],
  },
  {
    sku: "768686478758",
    categories: [ 1935 ],
  },
  {
    sku: "768686344695",
    categories: [ 1935 ],
  },
  {
    sku: "768686344688",
    categories: [ 1935 ],
  },
  {
    sku: "100558",
    categories: [ 2437 ],
  },
  {
    sku: "94764",
    categories: [ 1999 ],
  },
  {
    sku: "37955",
    categories: [ 2005 ],
  },
  {
    sku: "100388",
    categories: [ 2004 ],
  },
  {
    sku: "768686106057",
    categories: [ 1995 ],
  },
  {
    sku: "768686106002",
    categories: [ 1995 ],
  },
  {
    sku: "109125",
    categories: [ 2004 ],
  },
  {
    sku: "107176",
    categories: [ 2027 ],
  },
  {
    sku: "100154",
    categories: [ 2027 ],
  },
  {
    sku: "768686058370",
    categories: [ 1977 ],
  },
  {
    sku: "768686587122",
    categories: [ 1977 ],
  },
  {
    sku: "768686058387",
    categories: [ 1977 ],
  },
  {
    sku: "768686598838",
    categories: [ 1977 ],
  },
  {
    sku: "768686677670",
    categories: [ 1977 ],
  },
  {
    sku: "768686691225",
    categories: [ 1977 ],
  },
  {
    sku: "768686587139",
    categories: [ 1977 ],
  },
  {
    sku: "723660997700",
    categories: [ 1977 ],
  },
  {
    sku: "624316085881",
    categories: [ 1977 ],
  },
  {
    sku: "93808",
    categories: [ 1972 ],
  },

];


const drinkware = [
  '105344',
  '105345',
  '105343',
  '105342',
  '105341',
  '105340',
  '105339',
  '100014',
  '100139',
  '100133',
  '82206',
  '82205',
  '70493',
  '101148',
  '101149',
  '101147',
  '100141',
  '100131',
  '100130',
  '100129',
  '886798026138',
  '886798025674',
  '886798908335',
  '886798036410',
  '886798034232',
  '886798034164',
  '886798031729',
  '886798024332',
  '886798025681',
  '886798020433',
  '886798015880',
  '886798011202',
  '886798009575',
  '886798004532',
  '886798002392',
];

drinkware.forEach(c => {
  arr.push({ sku: c, categories: [1972] });
});


const drivetrainSmall = [
  "624316108573",
  "624316108580",
  "624316108542",
  "624316108528",
  "624316108511",
  "768114174337",
  "768114174177",
  "624316005865",
  "4250612879549",
  "4250612879525",
  "4055822041911",
  "4055822028196"
];


const cockpitSmall = [
  "624316096658",
  "723661000232",
//  "723660997892",
//  "723661000232",
  "624316781806",
  "723660991715",
//  "624316531654",
//  "624316531579",
  "624316531661",
  "624316531715",
  "624316531692",
  "624316531685",
  "624316531753",
  "624316531739",
  "624316531722",
  "624316555797",
  "624316531777",
  "624316531760",
//  "624316699552",
  "624316740667",
  "624316740674",
  "624316740681",
//  "624316774181",
  "624316740698",
  "624316781769",
  "624316781776",
  "624316781783",
  "624316781790",
  "624316781806",
  "624316781813",
  "100678",
  "723661003127",
  "723661003097",
  "723661001604",
  "723660997717",
  "723660993597",
  "723660985882",
  "723660982638",
  "723660005894",
  "723660005696",
  "723660005542",
  "723660005559",
  "723660005511",
  "723660005535",
  "723660005528",
  "624316517108",
  "624316517092",
  "624316125662",
  "624316111856",
  "624316125570",
  "624316125587",
//  "624316062295",
  "624316070023",
  "624316070030",
  "624316060253",
  "000007166648",
//  "170888",
//  "710845709685",
//  "710845709708",
  "710845709715",
  "4500",
  "624316062455",
  "624316062448",
  "624316062417",
  "624316062400",
  "4250612880583",
  "4055822036153",
  "610990330096",
  "10610990320018",
  "723661003059",
  "723661003042",
  "641740188506",
  "624316531531",
  "624316531517",
  "624316525103",
  "624316525110",
  "624316517153",
  "624316517146",
  "000002000008",
  "624316060284",
  "624316060277",
  "624316060260",
  "624316531500",
  "624316531494",
  "000002000244",
  "81181",
  "3199",
];

const tools = [
  '036121711211',
  '036121311107'
];



tools.forEach(c => {
  arr.push({ sku: c, categories: [1914] });
});


drivetrainSmall.forEach(c => {
  arr.push({ sku: c, categories: [1812] });
});


cockpitSmall.forEach(c => {
  arr.push({ sku: c, categories: [1868] });
});


module.exports = {
  supplier: supplier,
  rows: arr.map(obj => {
    return obj;
    if (obj.categories) { obj.categories.push(supplier); } return obj;
  })
};

