const supplier = 1731;

const arr = [
  {
    sku: "4713250834511",
    categories: [ 1868],
  },
  {
    sku: "4713250007106",
    categories: [ 1868],
  },
  {
    sku: "4713250850757",
    categories: [ 1868],
  },
  {
    sku: "4718905976950",
    categories: [ 1868],
  },
  {
    sku: "4718905992943",
    categories: [ 1913],
  },
  {
    sku: "4713250831497",
    categories: [ 1952],
  },
  {
    sku: "g-3547",
    categories: [ 2005],
  },
  {
    sku: "g-3347",
    categories: [ 2005],
  },
  {
    sku: "g-3135",
    categories: [ 2005],
  },
  {
    sku: "g-3134",
    categories: [ 2005],
  },
  {
    sku: "g-3132",
    categories: [ 2005],
  },
  {
    sku: "g-3389",
    categories: [ 2008],
  },
  {
    sku: "g-3390",
    categories: [ 2008],
  },
  {
    sku: "g-3148",
    categories: [ 2008],
  },
  {
    sku: "g-3547",
    categories: [ 2005],
  },
  {
    sku: "g-3347",
    categories: [ 2005],
  },
  {
    sku: "g-3135",
    categories: [ 2005],
  },
  {
    sku: "g-3134",
    categories: [ 2005],
  },
  {
    sku: "g-3132",
    categories: [ 2005],
  },
  {
    sku: "g-3132",
    categories: [ 2005],
  },
  {
    sku: "g-2231",
    categories: [ 2005],
  },
  {
    sku: "g-1954",
    categories: [ 2021],
  },
  {
    sku: "4718905955955",
    categories: [ 2430],
  },
  {
    sku: "4718905989752",
    categories: [ 1828],
  },
  {
    sku: "4718905989769",
    categories: [ 1828],
  },
  {
    sku: "4713250803944",
    categories: [ 1828],
  },
//   {
//     sku: "04718905894353",
//     categories: [ 1911 ],
//   },
//   {
//     sku: "04718905893837",
//     categories: [ 1940 ],
//   },
  {
    sku: "4718905976950",
    categories: [ 1868 ],
  },
  {
    sku: "4713250014586",
    categories: [ 2425 ],
  },
  {
    sku: "4713250014722",
    categories: [ 2425 ],
  },
  {
    sku: "4713250014586",
    categories: [ 1814 ],
  },
  {
    sku: "4713250014722",
    categories: [ 1814 ],
  },
  {
    sku: "g-1182",
    categories: [ 1870 ],
  },
  {
    sku: "g-3360",
    categories: [ 2005 ],
  },
  {
    sku: "g-3355",
    categories: [ 2005 ],
  },
  {
    sku: "g-2741",
    categories: [ 2005 ],
  },
  {
    sku: "4718905900740",
    categories: [ 1954 ],
  },


];

const cockpitSmall = [
  '4718905969785',
  '4718905960010',
];

cockpitSmall.forEach(c => {
  arr.push({ sku: c, categories: [1868] });
});



module.exports = {
  supplier: supplier,
  rows: arr.map(obj => {
    return obj;
    if (obj.categories) { obj.categories.push(supplier); } return obj;
  })
};
