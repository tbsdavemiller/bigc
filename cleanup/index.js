const
  config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  sleep = require('../utils/sleep'),
  upload = require(path.join(__dirname, '..', 'product'));


const { readdirSync } = require('fs');

let sources = process.argv.slice(2).map(s => { return s.split('/')[0]; });

if (sources.length == 0) {
  sources = fs.readdirSync(
    __dirname, { withFileTypes: true }
  )
  .filter((item) => item.isDirectory())
  .map((item) => item.name);
}

let i = 0;
let errors = 0;

sources.forEach(source => {

  let updates;
  try {
    updates = require(path.join(__dirname, source));
  } catch (e) {
    console.error(`🚫 invalid supplier`, e);
    return;
  }

  if (updates.rows.length == 0) {
    return;
  }

  updates.rows.forEach(obj => {
    if (!obj.upc && ! obj.sku) {
      return;
    }
    i++;
    sleep(2000)
    .then(() => {
//      console.log('.');
      return upload(obj, {env: 'prod'});
    })
    .then(product => {
//      console.log(`${sources.length == 1 ? '' : `${source} `}✅ `, obj);
    }).
    catch(e => {
//    console.log(e);
      errors++;
      console.error(`${sources.length == 1 ? '' : `${source} `}🚫`, obj.sku, errors);
    });
  })

});

if (i > 0) {
  console.log(i);
}
