const
  config = require('../config'),
  path = require('path'),
  fs = require('fs'),
  sleep = require('../utils/sleep'),
  upload = require(path.join(__dirname, '..', 'product'));


const { readdirSync } = require('fs');

let sources = process.argv.slice(2).map(s => { return s.split('/')[0]; });

if (sources.length == 0) {

//     console.error(`🚫 no supplier provided`);
//     return;

  sources = fs.readdirSync(
    __dirname, { withFileTypes: true }
  )
  .filter((item) => item.isDirectory())
  .map((item) => item.name);
}

let i = 0;

let keys = [
  'sku_id',
  'sku_title',
  'sku_quantity_available',
  'sku_cost',
  'sku_description',
  'sku_manufacturer',
  'sku_map',
  'sku_msrp',
  'sku_mpn',
  'sku_upc',
  'sku_length',
  'sku_width',
  'sku_height',
  'sku_weight',
  'sku_handling_cost',
  'sku_is_visible',
  'attribute_name1',
  'attribute_value1',
  'attribute_name2',
  'attribute_value2',
  'attribute_name3',
  'attribute_value3',
  'image_url1',
  'image_url2',
  'image_url3',
  'image_url4',
  'image_url5',
  'category_level1',
  'category_level2',
  'category_level3',
  'category_level4'
];


function keyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}

function allByValue(object, value) {
  return Object.keys(object).find(key => object[key].all === value);
}

let cats = function() {
  let C = require('../config/_categories');

  let result = {};

  Object.keys(C).forEach(k1 => {

    Object.keys(C[k1]).forEach(k2 => {

      if (k2 == 'all') {
        return;
      }

      if (! (C[k1][k2] instanceof Object)) {

        result['' + C[k1][k2]] = {
          category_level1: C[k1].all,
          category_level2: C[k1][k2],
          category_level3: '',
          category_level4: ''
        };
      }

      Object.keys(C[k1][k2]).forEach(k3 => {
        if (k3 === 'all') {
          return;
        }

        result['' + C[k1][k2][k3]] = {
          category_level1: allByValue(C, C[k1].all),
          category_level2: allByValue(C[k1], C[k1][k2].all),
          category_level3: keyByValue(C[k1][k2], C[k1][k2][k3]),
          category_level4: ''
        };
      });
    });
  });
  return result;
};

const CATS = cats();

console.log(keys.join(','));

sources.forEach(source => {

  let updates;
  try {
    updates = require(path.join(__dirname, source));
  } catch (e) {
    console.error(`🚫 invalid supplier`, e);
    return;
  }

  if (updates.rows.length == 0) {
    return;
  }

  updates.rows.forEach(obj => {
    if (!obj.sku) {
      console.error(`🚫 no sku for ${source} ${JSON.stringify(obj)}`);
      return;
    }
    let row = {
      sku_id: obj.sku
    };
    if (obj.is_visible === false) {
      row.sku_is_visible = false;
    }
    if (obj.categories) {
      let c = CATS['' + obj.categories[0]];
      row = {...row, ...c};
    }

    let buf = '';
    keys.forEach(k => {
      buf += (row.hasOwnProperty(k) ? `"${row[k]}"` : '') + ',';
    });

    console.log(buf);
    i++;
  })
});

if (i > 0) {
//  console.log(i);
}
